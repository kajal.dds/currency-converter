package com.example.currencyconverter.graph;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.currencyconverter.R;
import com.example.currencyconverter.apiinterface.IMarker;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

@SuppressLint("ViewConstructor")
public class CustomMarkerView extends MarkerView implements IMarker {

    private TextView tvContent;
    ArrayList<String> mXlables;
    private MPPointF mOffset = new MPPointF();
    private MPPointF mOffset2 = new MPPointF();
    private WeakReference<Chart> mWeakChart;
    int xpos;
    private Context mContext;

    public CustomMarkerView(Context context, int layoutResource, ArrayList<String> xlables) {
        super(context,layoutResource);
        mXlables = xlables;
        tvContent =  findViewById(R.id.tvContent);
        this.mContext = context;
    }

    @Override
    public MPPointF getOffset() {
        return mOffset;
    }

    public Chart getChartView() {
        return mWeakChart == null ? null : mWeakChart.get();
    }

    @Override
    public MPPointF getOffsetForDrawingAtPoint(float posX, float posY) {
        MPPointF offset = getOffset();
        mOffset2.x = offset.x;
        mOffset2.y = offset.y;

        Chart chart = getChartView();

        float width = getWidth();
        float height = getHeight();

        if (posX + mOffset2.x < 0) {
            mOffset2.x = - posX;
        } else if (chart != null && posX + width + mOffset2.x > chart.getWidth()) {
            mOffset2.x = chart.getWidth() - posX - width;
        }

        if (posY + mOffset2.y < 0) {
            mOffset2.y = - posY;
        } else if (chart != null && posY + height + mOffset2.y > chart.getHeight()) {
            mOffset2.y = chart.getHeight() - posY - height;
        }

        return mOffset2;
    }

    public void setChartView(Chart chart) {
        mWeakChart = new WeakReference<>(chart);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        tvContent.setText( mXlables.get(e.getXIndex())+ "\n" + e.getVal());
         xpos = e.getXIndex();
        Log.e("hello", "refreshContent: " +tvContent.getText().toString());
    }

    /*@Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }*/

    @Override
    public int getXOffset(float v) {
//        return -(getWidth());

        int min_offset = 100;
        if (xpos < min_offset)
            return 0;

        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        //For right hand side
        if (metrics.widthPixels - xpos > min_offset)
            return -getWidth();
            //For left hand side
        else if (metrics.widthPixels - xpos < 0)
            return -getWidth();
        return -(getWidth() / 2);
    }

    @Override
    public int getYOffset(float v) {
        // this will cause the marker-view to be above the selected value
        return -getHeight();
//        return -(int)(v);
    }
}
