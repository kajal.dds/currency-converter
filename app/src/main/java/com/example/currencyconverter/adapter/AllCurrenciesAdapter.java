package com.example.currencyconverter.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.currencyconverter.R;
import com.example.currencyconverter.activity.AllCurrenciesActivity;
import com.example.currencyconverter.activity.CurrencyProfileActivity;
import com.example.currencyconverter.activity.MainActivity;
import com.example.currencyconverter.model.CurrencyModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.example.currencyconverter.activity.MainActivity.ivFlag;
import static com.example.currencyconverter.activity.MainActivity.selectedItems;
import static com.example.currencyconverter.activity.MainActivity.tempList;
import static com.example.currencyconverter.activity.MainActivity.tvCurrencyCode;
import static com.example.currencyconverter.activity.MainActivity.tvCurrencyName;
import static com.example.currencyconverter.activity.MainActivity.tvSymbol;


public class AllCurrenciesAdapter extends RecyclerView.Adapter<AllCurrenciesAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<CurrencyModel> currencyModelArrayList;
    private static final String TAG = "AllCurrenciesAdapter";

    AllCurrenciesAdapter(Context mContext, ArrayList<CurrencyModel> currencyModelArrayList) {
        this.mContext = mContext;
        this.currencyModelArrayList = currencyModelArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_all_currency, parent, false);

        return new MyViewHolder(v);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        final SharedPreferences mSharedPreferences = mContext.getSharedPreferences("clickCount", MODE_PRIVATE);
        final SharedPreferences.Editor mEditor = mSharedPreferences.edit();

        final SharedPreferences mSharedPreferences1 = mContext.getSharedPreferences("convert", MODE_PRIVATE);
        final SharedPreferences.Editor mEditor1 = mSharedPreferences1.edit();
        mEditor1.apply();

        Glide.with(mContext)
                .load(currencyModelArrayList.get(position).getFlag())
                .into(holder.ivFlag);
        holder.tvCurrencyCode.setText(currencyModelArrayList.get(position).getCurrencyCode());
        holder.tvCurrencyName.setText(currencyModelArrayList.get(position).getCurrencyName());

      /*  if (currencyModelArrayList.get(position).getFlag() == R.drawable.united_states_of_america ||
                currencyModelArrayList.get(position).getFlag() == R.drawable.canada) {

            MainActivity.selectedItems.put(currencyModelArrayList.get(position).getFlag(), true);
        }*/

        SharedPreferences sharedPreferences1 = mContext.getSharedPreferences("HashMap", MODE_PRIVATE);
        String defValue = new Gson().toJson(new SparseBooleanArray());
        String json = sharedPreferences1.getString("map", defValue);
        TypeToken<HashMap<String, Boolean>> token = new TypeToken<HashMap<String, Boolean>>() {
        };
        selectedItems = new Gson().fromJson(json, token.getType());

        if (!MainActivity.isFromGraphsource && !MainActivity.isFromdest && !CurrencyProfileActivity.isFromCurrencyProfile) {
            Log.e(TAG, "onBindViewHolder: not from graph");
            for (String m : selectedItems.keySet()) {
                Log.e(TAG, "onBindViewHolder: somethinggg" + m);
                if (currencyModelArrayList.get(position).getCurrencyCode().equals(m)) {
                    Log.e(TAG, "onBindViewHolder: posss" + position);
                    Log.e(TAG, "onBindViewHolder: poppp" + currencyModelArrayList.get(position).getCurrencyCode());
                    Log.e(TAG, "onBindViewHolder: yay");
                    holder.ivSelect.setVisibility(View.VISIBLE);
                }
            }
        } else {
            Log.e(TAG, "onBindViewHolder: not from all");
            Log.e(TAG, "onBindViewHolder: " + MainActivity.isFromdest);
            Log.e(TAG, "onBindViewHolder: " + MainActivity.isFromGraphsource);
            Log.e(TAG, "onBindViewHolder: " + CurrencyProfileActivity.isFromCurrencyProfile);
            if (holder.ivSelect.isShown()) {
                holder.ivSelect.setVisibility(View.GONE);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                if (!MainActivity.isFromGraphsource && !MainActivity.isFromdest && !CurrencyProfileActivity.isFromCurrencyProfile) {

                    Log.e(TAG, "onClick: none" );
                    mEditor.putBoolean("isClicked", true);

                    if (holder.ivSelect.isShown()) {
                        Log.e(TAG, "onClickkkkkkkkkk: is shown ===>");

                        if (selectedItems.size() > 3) {

                            if (currencyModelArrayList.get(position).getCurrencyCode().equals(tvCurrencyCode.getText().toString())) {
                                Log.e(TAG, "onClick: yay match found");

                                SharedPreferences sharedPreferences = mContext.getSharedPreferences("swapData", MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                tvCurrencyName.setText(tempList.get(0).getCurrencyName());
                                tvCurrencyCode.setText(tempList.get(0).getCurrencyCode());
                                tvSymbol.setText(tempList.get(0).getCurrencySymbol());
                                MainActivity.ivFlag.setImageDrawable(mContext.getDrawable(tempList.get(0).getFlag()));
                                MainActivity.ivFlag.setTag(tempList.get(0).getFlag());

                                editor.putString("swapName", tvCurrencyName.getText().toString());
                                editor.putString("swapCode", tvCurrencyCode.getText().toString());
                                editor.putString("swapSymbol", tvSymbol.getText().toString());
                                editor.putInt("swapFlag", (int) ivFlag.getTag());
                                editor.apply();

                                SharedPreferences sharedPreferences1 = mContext.getSharedPreferences("clickCount", MODE_PRIVATE);
                                SharedPreferences.Editor editor1 = sharedPreferences1.edit();

                                Map<String, ?> keys = sharedPreferences1.getAll();
                                for (Map.Entry<String, ?> entry : keys.entrySet()) {
                                    Log.e("map values", "map values" + entry.getKey() + ": " + entry.getValue().toString());
                                    editor1.remove("C0");
                                    editor1.remove("C01");
                                    editor1.apply();
                                }
                            }

                            Log.e(TAG, "onClick: ==> size" + selectedItems.size());

                            Map<String, ?> keys = mSharedPreferences.getAll();
                            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                                Log.e("map values", "map values" + entry.getKey() + ": " + entry.getValue().toString());
                                if (/*currencyModelArrayList.get(position).getFlag().equals(entry.getValue()) ||*/
                                        (entry.getValue().toString().substring(0, 3).equals(currencyModelArrayList.get(position).getCurrencyCode()))) {
                                    mEditor.remove(entry.getKey());
                                    mEditor.apply();
                                }
                            }
                            holder.ivSelect.setVisibility(View.GONE);
                            MainActivity.selectedItems.remove(currencyModelArrayList.get(position).getCurrencyCode());
                            String jsonString = new Gson().toJson(MainActivity.selectedItems);
                            SharedPreferences sharedPreferences = mContext.getSharedPreferences("HashMap", MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("map", jsonString);
                            editor.apply();
                        } else {
                            Toast.makeText(mContext, "AtLeast 3 items required", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Log.e(TAG, "onClick: is shown not ===>");
                        if (MainActivity.selectedItems.size() < 11) {
                            Log.e(TAG, "onClick: ==> size " + selectedItems.size());

                            String[] s1 = {currencyModelArrayList.get(position).getCurrencyCode(), currencyModelArrayList.get(position).getCurrencyName(),
                                    currencyModelArrayList.get(position).getCurrencySymbol()};
                            StringBuilder sb = new StringBuilder();
                            for (String s : s1) {
                                sb.append(s).append(",");
                            }

                            if (mSharedPreferences.contains("C2")) {
                                if (mSharedPreferences.contains("C3")) {
                                    if (mSharedPreferences.contains("C4")) {
                                        if (mSharedPreferences.contains("C5")) {
                                            if (mSharedPreferences.contains("C6")) {
                                                if (mSharedPreferences.contains("C7")) {
                                                    if (mSharedPreferences.contains("C8")) {
                                                        if (mSharedPreferences.contains("C9")) {
                                                            if (mSharedPreferences.contains("C10")) {
                                                                Toast.makeText(mContext, "Can't add more then 10 data", Toast.LENGTH_SHORT).show();
                                                            } else {
                                                                mEditor.putString("C10", sb.toString());
                                                                mEditor.putInt("C101", currencyModelArrayList.get(position).getFlag());
                                                                mEditor.apply();
                                                            }
                                                        } else {
                                                            mEditor.putString("C9", sb.toString());
                                                            mEditor.putInt("C91", currencyModelArrayList.get(position).getFlag());
                                                            mEditor.apply();
                                                        }
                                                    } else {
                                                        mEditor.putString("C8", sb.toString());
                                                        mEditor.putInt("C81", currencyModelArrayList.get(position).getFlag());
                                                        mEditor.apply();
                                                    }
                                                } else {
                                                    mEditor.putString("C7", sb.toString());
                                                    mEditor.putInt("C71", currencyModelArrayList.get(position).getFlag());
                                                    mEditor.apply();
                                                }
                                            } else {
                                                mEditor.putString("C6", sb.toString());
                                                mEditor.putInt("C61", currencyModelArrayList.get(position).getFlag());
                                                mEditor.apply();
                                            }
                                        } else {
                                            mEditor.putString("C5", sb.toString());
                                            mEditor.putInt("C51", currencyModelArrayList.get(position).getFlag());
                                            mEditor.apply();
                                        }
                                    } else {
                                        mEditor.putString("C4", sb.toString());
                                        mEditor.putInt("C41", currencyModelArrayList.get(position).getFlag());
                                        mEditor.apply();
                                    }
                                } else {
                                    mEditor.putString("C3", sb.toString());
                                    mEditor.putInt("C31", currencyModelArrayList.get(position).getFlag());
                                    mEditor.apply();
                                }
                            } else {
                                mEditor.putString("C2", sb.toString());
                                mEditor.putInt("C21", currencyModelArrayList.get(position).getFlag());
                                mEditor.apply();
                            }

                            holder.ivSelect.setVisibility(View.VISIBLE);
                            MainActivity.currencyAdapter.notifyDataSetChanged();
                            MainActivity.selectedItems.put(currencyModelArrayList.get(position).getCurrencyCode(), true);
                            String jsonString = new Gson().toJson(MainActivity.selectedItems);
                            SharedPreferences sharedPreferences = mContext.getSharedPreferences("HashMap", MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("map", jsonString);
                            editor.apply();

                        } else {
                            Toast.makeText(mContext, "can't select more then 10 items", Toast.LENGTH_SHORT).show();
                        }
                    }
                    AllCurrenciesActivity.alphabetsAdapter.notifyDataSetChanged();
                    Log.e(TAG, "onClick: tempList size" + tempList.size());
                } else {
                    if (MainActivity.isFromGraphsource) {
                        Log.e(TAG, "onClickkkkkkkkkk: from src");
                        Log.e(TAG, "onClick: src"+currencyModelArrayList.get(position).getCurrencyCode() );
                        mEditor1.putString("graphSource", currencyModelArrayList.get(position).getCurrencyCode());
                        mEditor1.putInt("sourceFlag", currencyModelArrayList.get(position).getFlag());
                        mEditor1.apply();
                        ((Activity) mContext).finish();
                    }

                    if (MainActivity.isFromdest) {
                        Log.e(TAG, "onClickkkkkkkkkk: from dest");
                        Log.e(TAG, "onClick: dest"+currencyModelArrayList.get(position).getCurrencyCode());
                        mEditor1.putString("graphDest", currencyModelArrayList.get(position).getCurrencyCode());
                        mEditor1.putInt("destFlag", currencyModelArrayList.get(position).getFlag());
                        mEditor1.apply();
                        ((Activity) mContext).finish();
                    }

                    if (CurrencyProfileActivity.isFromCurrencyProfile) {
                        CurrencyProfileActivity.isFromCurrencyProfile = false;
                        mEditor.putInt("cpflag", currencyModelArrayList.get(position).getFlag());
                        mEditor.putString("code", currencyModelArrayList.get(position).getCurrencyCode());
                        mEditor.putString("currencyName", currencyModelArrayList.get(position).getCurrencyName());
                        mEditor.putString("symbol", currencyModelArrayList.get(position).getSymbol());
                        mEditor.putString("bank", currencyModelArrayList.get(position).getBankName());
                        mEditor.putString("banknotes", currencyModelArrayList.get(position).getBankNotes());
                        mEditor.putString("coins", currencyModelArrayList.get(position).getCoins());
                        mEditor.apply();
                        ((Activity) mContext).finish();
                    }
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return currencyModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivFlag, ivSelect;
        private TextView tvCurrencyCode, tvCurrencyName;
        View itemView;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            ivFlag = itemView.findViewById(R.id.item_iv_flag);
            ivSelect = itemView.findViewById(R.id.iv_select);
            tvCurrencyCode = itemView.findViewById(R.id.item_tv_currency_code);
            tvCurrencyName = itemView.findViewById(R.id.item_tv_currency_name);
        }
    }

}
