package com.example.currencyconverter.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.currencyconverter.R;
import com.example.currencyconverter.model.CreateAlertModel;
import com.google.gson.Gson;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class CreateAlertAdapter extends RecyclerView.Adapter<CreateAlertAdapter.MyViewHolder> {

    private ArrayList<CreateAlertModel> list;
    private Context mContext;


    public CreateAlertAdapter(Context mContext, ArrayList<CreateAlertModel> list) {
        this.list = list;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_create_alert, parent, false);
        return new MyViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.tvSource.setText(list.get(position).getWhen());
        holder.tvDest.setText(list.get(position).getEquals());
        holder.tvAlertAmount.setText(Double.toString(list.get(position).getAmount()));
        holder.tvCurrentPrice.setText(list.get(position).getCurrentPrice());

        if(position == (list.size()-1)){
            holder.line.setVisibility(View.GONE);
        }

        holder.ivDeleteAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("alert adapter", "onClick: before"+list.size() );
                list.remove(position);

                 SharedPreferences sharedPreferences;
                 SharedPreferences.Editor mEditor;

                sharedPreferences = mContext.getSharedPreferences("clickCount", MODE_PRIVATE);
                mEditor = sharedPreferences.edit();
                mEditor.apply();

                Gson gson = new Gson();
                String json = gson.toJson(list);
                mEditor.putString("list", json);
                mEditor.apply();

                notifyDataSetChanged();
                Log.e("alert adapter", "onClick: after"+list.size() );
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

     class MyViewHolder extends RecyclerView.ViewHolder{

        TextView tvSource , tvDest, tvCurrentPrice, tvAlertAmount;
        View line;
        ImageView ivDeleteAlert;

         MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvCurrentPrice = itemView.findViewById(R.id.tv_row_item_alert_current_price);
            tvSource = itemView.findViewById(R.id.tv_row_item_alert_source);
            tvDest = itemView.findViewById(R.id.tv_row_item_alert_dest);
            tvAlertAmount = itemView.findViewById(R.id.tv_row_item_alert_amount);
            line = itemView.findViewById(R.id.line);
            ivDeleteAlert = itemView.findViewById(R.id.iv_delete_alert);
        }
    }
}
