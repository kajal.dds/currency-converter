package com.example.currencyconverter.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.currencyconverter.R;
import com.example.currencyconverter.model.MainModel;

import java.util.ArrayList;
import java.util.List;

public class AlphabetsAdapter extends RecyclerView.Adapter<AlphabetsAdapter.MyViewHolder> implements Filterable {

    private Context mContext;
    private ArrayList<MainModel> mainModelArrayList;
    private MainModel items;

    private static final String TAG = "AlphabetsAdapter";


    public AlphabetsAdapter(Context context, ArrayList<MainModel> mainModelArrayList) {
        this.mContext = context;
        this.mainModelArrayList = mainModelArrayList;
//        this.alphabetList = alphabetList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_alphabets, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.tvAlphabet.setText(mainModelArrayList.get(position).getLetter());
        Log.e(TAG, "onBindViewHolder: "+mainModelArrayList.get(position).getLetter());

        items = mainModelArrayList.get(position);
//        ArrayList<CurrencyModel> currencyModelArrayList = mainModelArrayList.get(position).getCurrencyModelArrayList();
//        Log.e(TAG, "onBindViewHolder: "+mainModelArrayList.get(position).getCurrencyModelArrayList().get(position));
            AllCurrenciesAdapter adapter = new AllCurrenciesAdapter(mContext, mainModelArrayList.get(position).getCurrencyModelArrayList());
            holder.rvAllCurrencies.setAdapter(adapter);


    }

    @Override
    public int getItemCount() {
        return mainModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvAlphabet;
        private RecyclerView rvAllCurrencies;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvAlphabet = itemView.findViewById(R.id.tv_alphabet);
            rvAllCurrencies = itemView.findViewById(R.id.rv_all_currency);

            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            rvAllCurrencies.setLayoutManager(layoutManager);

        }
    }


    public Filter getFilter(){
        return new Filter() {
            @SuppressLint("Assert")
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<MainModel> results = new ArrayList<>();
                if (mainModelArrayList == null) {
                    assert false;
                    mainModelArrayList.add(items);
                }
                if (constraint != null){
                    if(mainModelArrayList.size() > 0){
                        for ( final MainModel g :mainModelArrayList) {
                            if (g.getCurrencyModelArrayList().contains(constraint.toString())) {
                                results.add(g);
                            }
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                items = (MainModel) results.values;
                notifyDataSetChanged();
            }
        };
    }


}

