package com.example.currencyconverter.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.currencyconverter.R;
import com.example.currencyconverter.model.CurrencyModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.example.currencyconverter.activity.MainActivity.dbGetValue;
import static com.example.currencyconverter.activity.MainActivity.etCurrencyValue;
import static com.example.currencyconverter.activity.MainActivity.ivFlag;
import static com.example.currencyconverter.activity.MainActivity.language;
import static com.example.currencyconverter.activity.MainActivity.tvCurrencyCode;
import static com.example.currencyconverter.activity.MainActivity.tvCurrencyName;
import static com.example.currencyconverter.activity.MainActivity.tvSymbol;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<CurrencyModel> currencyModelArrayList;

    private String strName, strCode, strSymbol, strPrice;
    private Object flag;


    public CurrencyAdapter(Context mContext, ArrayList<CurrencyModel> currencyModelArrayList) {
        this.mContext = mContext;
        this.currencyModelArrayList = currencyModelArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_currency, parent, false);
        return new CurrencyAdapter.MyViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

//        holder.tvCurrencyValue.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(6,2)});

        holder.ivFlag.setImageDrawable(mContext.getDrawable(currencyModelArrayList.get(position).getFlag()));
        holder.tvCurrencySymbol.setText(currencyModelArrayList.get(position).getCurrencySymbol());
        holder.tvCurrencyName.setText(currencyModelArrayList.get(position).getCurrencyName());
        holder.tvCurrencyCode.setText(currencyModelArrayList.get(position).getCurrencyCode());
        holder.tvPrice.setText("" + String.format(Locale.getDefault(), "%.3f", currencyModelArrayList.get(position).getCurrencyPrice()));
        holder.tvCurrencyValue.setText(currencyModelArrayList.get(position).getCurrencyValue());

        if(currencyModelArrayList!=null){
            getJsonData();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = mContext.getSharedPreferences("swapData", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.putBoolean("swapDataClick", true);
                editor.apply();

                // TODO: 29-11-2019 get and save textView data
                //////////////////////////////////////////////////////////////////////////////////////
                strName = tvCurrencyName.getText().toString();
                strCode = tvCurrencyCode.getText().toString();
                strSymbol = tvSymbol.getText().toString();
                strPrice = etCurrencyValue.getText().toString();
                flag = ivFlag.getTag();

                editor.putString("oldName", strName);
                editor.putString("oldCode", strCode);
                editor.putString("oldSymbol", strSymbol);
                editor.putInt("oldFlag", (int) flag);
                editor.putString("oldValue", strPrice);
                editor.apply();

                // TODO: 29-11-2019 set clicked item data to textview and save
                tvCurrencyName.setText(currencyModelArrayList.get(position).getCurrencyName());
                tvCurrencyCode.setText(currencyModelArrayList.get(position).getCurrencyCode());
                tvSymbol.setText(currencyModelArrayList.get(position).getCurrencySymbol());
                etCurrencyValue.setText("");
                etCurrencyValue.setText(Double.toString(currencyModelArrayList.get(position).getCurrencyPrice()));
                ivFlag.setImageDrawable(mContext.getDrawable(currencyModelArrayList.get(position).getFlag()));
                ivFlag.setTag(currencyModelArrayList.get(position).getFlag());

                editor.putString("swapName", tvCurrencyName.getText().toString());
                editor.putString("swapCode", tvCurrencyCode.getText().toString());
                editor.putString("swapSymbol", tvSymbol.getText().toString());
                editor.putString("swapPrice", etCurrencyValue.getText().toString());
                editor.putInt("swapFlag", (int) ivFlag.getTag());
                editor.apply();

                // TODO: 29-11-2019 set textView's data to clicked item
                currencyModelArrayList.get(position).setCurrencyName(sharedPreferences.getString("oldName", "default name"));
                currencyModelArrayList.get(position).setCurrencyCode(sharedPreferences.getString("oldCode", "default code"));
                currencyModelArrayList.get(position).setCurrencySymbol(sharedPreferences.getString("oldSymbol", "default symbol"));
                currencyModelArrayList.get(position).setFlag(sharedPreferences.getInt("oldFlag", -99));
                currencyModelArrayList.get(position).setCurrencyPrice(Double.parseDouble(sharedPreferences.getString("oldValue", "default value")));
                notifyDataSetChanged();

                // TODO: 29-11-2019 save clicked item data
                String[] data = {strCode, strName, strSymbol, strPrice};
                StringBuilder sb = new StringBuilder();
                for (String datum : data) {
                    sb.append(datum).append(",");
                }

                SharedPreferences sharedPreferences1 = mContext.getSharedPreferences("clickCount", MODE_PRIVATE);
                SharedPreferences.Editor editor1 = sharedPreferences1.edit();
                Map<String, ?> keys = sharedPreferences1.getAll();
                for (Map.Entry<String, ?> entry : keys.entrySet()) {
                    Log.e("map values", "map values" + entry.getKey() + ": " + entry.getValue().toString());
                    Log.e("map values", "onClick: this is substring" + entry.getValue().toString().substring(0, 3));
                    if (entry.getValue().toString().substring(0, 3).equals(tvCurrencyCode.getText().toString())) {
                        editor1.putString(entry.getKey(), sb.toString());
                        editor1.apply();
                    }
                    if (entry.getValue().equals(ivFlag.getTag())) {
                        editor1.putInt(entry.getKey(), (int) flag);
                        editor1.commit();
                    }
                }

                for (int i = 0; i < currencyModelArrayList.size(); i++) {
                    dbGetValue = Double.parseDouble(etCurrencyValue.getText().toString()) / currencyModelArrayList.get(i).getCurrencyPrice();
//                    dbGetValue = Double.parseDouble(df.format(Double.parseDouble(etCurrencyValue.getText().toString()) / currencyModelArrayList.get(i).getCurrencyPrice()));
                    currencyModelArrayList.get(i).setCurrencyValue(("1" + currencyModelArrayList.get(i).getCurrencyCode() + " = " + String.format(Locale.getDefault(), "%.5f", dbGetValue) + " " + tvCurrencyCode.getText().toString()));
                    notifyDataSetChanged();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return currencyModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivFlag;
        private TextView tvCurrencyName, tvCurrencySymbol, tvCurrencyValue, tvCurrencyCode, tvPrice;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ivFlag = itemView.findViewById(R.id.item_currency_iv_flag);
            tvCurrencyName = itemView.findViewById(R.id.item_currency_tv_currency_name);
            tvCurrencySymbol = itemView.findViewById(R.id.item_currency_tv_currency_symbol);
            tvCurrencyValue = itemView.findViewById(R.id.item_currency_item_tv_currency_value);
            tvCurrencyCode = itemView.findViewById(R.id.item_currency_tv_currency_code);
            tvPrice = itemView.findViewById(R.id.item_currency_tv_price);
        }
    }

    private String loadJSON() {
        String json;
        try {
            InputStream is;
            if (language.equals("ar")) {
                is = mContext.getAssets().open("arabiccurrency");
            } else {
                is = mContext.getAssets().open("currencydetails");
            }
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
            Log.e("currency adapter", "loadJSON: response" + json);
        } catch (IOException ex) {
            ex.printStackTrace();
            Log.e("currency adapter", "loadJSON: catch" + ex.getMessage());
            return null;
        }
        return json;
    }

    private void getJsonData() {
        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(loadJSON());
            Log.e("currency adapter", "getJsonData: " + jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String currencyCode = jsonObject.getString("code");
                String currencyName = jsonObject.getString("name");
                for (int j = 0; j < currencyModelArrayList.size() ; j++) {
                    if(currencyModelArrayList.get(j).getCurrencyCode().equals(currencyCode)){
                        currencyModelArrayList.get(j).setCurrencyName(currencyName);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
