package com.example.currencyconverter.apiinterface;

import com.example.currencyconverter.model.CurrencyConversion;
import com.example.currencyconverter.model.GraphData;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @POST("convert")
    Call<CurrencyConversion> getConversion(@Query("access_key") String key, @Query("from") String source, @Query("to") String destination,
                                                 @Query("amount") Float amount, @Query("date") String date);

    @POST("timeframe")
    Call<GraphData> getData(@Query("access_key") String key,  @Query("source") String source, @Query("currencies") String destinantion,
                            @Query("start_date") String startDate, @Query("end_date") String endDate);
}
