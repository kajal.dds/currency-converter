package com.example.currencyconverter.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.currencyconverter.R;
import com.google.firebase.analytics.FirebaseAnalytics;

public class CurrencyProfileActivity extends AppCompatActivity {

    public static boolean isFromCurrencyProfile ;

    private SharedPreferences mSharedPreferences;
    private TextView tvName, tvSymbol, tvBank, tvBankNotes, tvCoins, tvCpName, tvCode;
    private ImageView ivFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setContentView(R.layout.activity_currency_profile);

        mSharedPreferences =getSharedPreferences("clickCount", MODE_PRIVATE);

        LinearLayout llSelectCurrency = findViewById(R.id.ll_select_currency);
        tvName = findViewById(R.id.tv_cp_currency_name);
        tvSymbol = findViewById(R.id.tv_cp_symbol);
        tvBank = findViewById(R.id.tv_cp_bank);
        tvBankNotes = findViewById(R.id.tv_cp_banknotes);
        tvCoins = findViewById(R.id.tv_cp_coins);
        tvCpName = findViewById(R.id.tv_cp_name);
        tvCode = findViewById(R.id.tv_cp_currency_code);
        ivFlag = findViewById(R.id.iv_cp_flag);
        ImageView ivBack = findViewById(R.id.iv_cp_back);

        if(MainActivity.language.equals("ar")){
            tvSymbol.setTextDirection(View.TEXT_DIRECTION_RTL);
        } else {
            tvSymbol.setTextDirection(View.TEXT_DIRECTION_LTR);
        }

        FirebaseAnalytics mFireBaseAnalytics = FirebaseAnalytics.getInstance(this);

        llSelectCurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromCurrencyProfile = true;
                Intent intent = new Intent(CurrencyProfileActivity.this, AllCurrenciesActivity.class);
                startActivity(intent);
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onResume() {
        super.onResume();

        ivFlag.setImageDrawable(getDrawable(mSharedPreferences.getInt("cpflag",R.drawable.india)));
        tvCode.setText(mSharedPreferences.getString("code",getResources().getString(R.string.abb_inr)));
        tvName.setText(mSharedPreferences.getString("currencyName",getResources().getString(R.string.indian_rupees)));
        tvCpName.setText(mSharedPreferences.getString("currencyName",getResources().getString(R.string.indian_rupees)));
        tvSymbol.setText(mSharedPreferences.getString("symbol",getResources().getString(R.string.Rs)));
        tvBank.setText(mSharedPreferences.getString("bank",getResources().getString(R.string.reserve_bank_of_india)));
        tvBankNotes.setText(mSharedPreferences.getString("banknotes",getResources().getString(R.string._1_2_5_10_20_50_100_200_500_2000)));
        tvCoins.setText(mSharedPreferences.getString("coins",getResources().getString(R.string._1_2_5_10)));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isFromCurrencyProfile = false;
    }
}
