package com.example.currencyconverter.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.os.ConfigurationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.currencyconverter.AppClass;
import com.example.currencyconverter.R;
import com.example.currencyconverter.adapter.CurrencyAdapter;
import com.example.currencyconverter.fragments.ChartFragment;
import com.example.currencyconverter.fragments.CreateAlertFragment;
import com.example.currencyconverter.fragments.CurrencyConversionFragment;
import com.example.currencyconverter.fragments.MoreFragment;
import com.example.currencyconverter.model.CurrencyModel;
import com.example.currencyconverter.utils.ObjectSerializer;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static com.example.currencyconverter.fragments.MoreFragment.isFromLanguage;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    //    private BottomAppBar bottomAppBar;
    @SuppressLint("StaticFieldLeak")
    public static TextView tvCurrencyName, tvCurrencyCode, tvSymbol;
    public TextView tvDateTime, tvAppName, tvFrom, tvTo;
    @SuppressLint("StaticFieldLeak")
    public static TextView etCurrencyValue;
    private TextView tv1, tv2, tv3, tv5, tv4, tv6, tv7, tv8, tv9, tv0, tvDot, tvDelete, tvDone, tvClearAll, tvPlus, tvMinus, tvDivide, tvMultiplie;
    private ImageView ivRefresh, ivChart, ivPercent, ivDots, ivCalculator, ivCreateAlert;
    public ImageView ivManualRefresh;
    @SuppressLint("StaticFieldLeak")
    public static ImageView ivFlag;
    public FloatingActionButton fabConvert, fabAdd;
    public LinearLayout llFragmentContainer, llCalculator;
    public CardView llDisplayCurrency;
    public RecyclerView rvCurrency;
    private CoordinatorLayout coordinatorLayout;
    @SuppressLint("StaticFieldLeak")
    public static CurrencyAdapter currencyAdapter;
    public static ArrayList<CurrencyModel> tempList = new ArrayList<>();
    public static HashMap<String, Boolean> selectedItems = new HashMap<>();
    //    private ArrayList<String> allPrices = new ArrayList<>();
    public static HashMap<String, String> allPrices = new HashMap<>();
    private ArrayList<String> currencyCodeslist = new ArrayList<>();
    private String ans, strGetEtValue;
    private ArrayList<Integer> a = new ArrayList<>();
    private String prev_ans = "";
    private boolean cal_val_clear = false;
    private boolean value, isFromChart, isFromCoversion, isFromAlert;
    private static final String TAG = "MainActivity";
    private ProgressDialog progressDialog;
    private Double dbValueToMultiply = 1.0;
    public static Double dbGetValue;
    private SharedPreferences priceSharedPreferences;
    private SharedPreferences.Editor priceEditor;
    private long timeDifference, timeForUpdation;
    public LinearLayout llGraphText, llGraphsource, llGraphDestination, cvCurrencyDisplay, llMoreOptions;
    private TextView tvGraphSource, tvGraphDestination, tvMoreOptions;
    private ImageView ivGraphSource, ivGraphDestination;
    public static boolean isFromGraphsource = false, isFromdest = false;
    @SuppressLint("StaticFieldLeak")
    public static MainActivity activity;
    public static String language;
    private CardView cvSwap;
    private SharedPreferences editorSharedPreferences;
    private SharedPreferences mSharedPreferences1;
    private SharedPreferences.Editor mEditor, editorSwap , mEditor1;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        findViews();
        onClicklisters();
        activity = MainActivity.this;
        FirebaseAnalytics mFireBaseAnalytics = FirebaseAnalytics.getInstance(this);

        Log.e(TAG, "onCreate: lang"+ ConfigurationCompat.getLocales(Resources.getSystem().getConfiguration()));

        // TODO: 18-12-2019 Onesignal notification 

        AppClass applicationInstance = AppClass.getInstance();

        // TODO: 20-12-2019 Convert app to other language  
        SharedPreferences sharedPreferencesLang = getSharedPreferences("Language", Context.MODE_PRIVATE);

        language = sharedPreferencesLang.getString("mylanguage", "en");
        Log.e(TAG, "onCreate: language" + language);
        setLocale(language);


        tvAppName.setText(getResources().getString(R.string.currencyconvert));
        tvFrom.setText(getResources().getString(R.string.from));
        tvTo.setText(getResources().getString(R.string.to));
        tvMoreOptions.setText(getResources().getString(R.string.more_options));

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        Date result = cal.getTime();
        Log.e(TAG, "OnCreate: one month ago" + result);

        priceSharedPreferences = getSharedPreferences("priceValue", MODE_PRIVATE);
        priceEditor = priceSharedPreferences.edit();
        priceEditor.apply();

        etCurrencyValue.setText(priceSharedPreferences.getString("etValue", "1"));
        ans = priceSharedPreferences.getString("ansValue", "1");

        editorSharedPreferences = getSharedPreferences("swapData", Context.MODE_PRIVATE);
        editorSwap = editorSharedPreferences.edit();
        editorSwap.apply();

        boolean swapFlag = editorSharedPreferences.getBoolean("swapDataClick", false);

        if (!swapFlag) {
            ivFlag.setTag(R.drawable.united_states_of_america);
        } else {
            tvCurrencyName.setText(editorSharedPreferences.getString("swapName", "default name"));
            tvCurrencyCode.setText(editorSharedPreferences.getString("swapCode", "default code"));
            tvSymbol.setText(editorSharedPreferences.getString("swapSymbol", "default symbol"));
            ivFlag.setImageDrawable(getDrawable(editorSharedPreferences.getInt("swapFlag", -1)));
            ivFlag.setTag(editorSharedPreferences.getInt("swapFlag", -1));
        }

        SharedPreferences mSharedPreferences = getSharedPreferences("clickCount", MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();

        mSharedPreferences1 = getSharedPreferences("convert", MODE_PRIVATE);
        mEditor1 = mSharedPreferences1.edit();
        mEditor1.apply();

        Log.e(TAG, "onCreate: hello");
        // TODO: 19-11-2019 different styles of characters managed in one string
        // tvCurrencyName.setText(Html.fromHtml(getString(R.string.usd)));
        value = mSharedPreferences.getBoolean("isClicked", false);

        Log.e(TAG, "onCreate: AppClass.count" + AppClass.count);

     /*   if (!isConnected(activity)) {
            Toast.makeText(activity, "Connect to Internet and Refresh", Toast.LENGTH_LONG).show();
        }
*/

        if (AppClass.count == 1) {
            getTime();
            if (!isFromLanguage) {
                if (isConnected(activity)) {
                    getJsonData();
                    new DisplayCurrencyValue().execute();
                    Log.e(TAG, "onCreate: DisplayCurrency 1st time");
                } else {
                    Toast.makeText(activity, "Connect to Internet and Refresh", Toast.LENGTH_LONG).show();
                }
            }

            SharedPreferences sharedPreferencesH = getSharedPreferences("HashMap", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferencesH.edit();

            CurrencyModel currencyModel = new CurrencyModel();
            currencyModel.setCurrencyName(getResources().getString(R.string.euro1));
            currencyModel.setCurrencyCode("EUR");
            currencyModel.setFlag(R.drawable.european_union);
            currencyModel.setCurrencySymbol("€");
            tempList.add(currencyModel);

            String[] s1 = {currencyModel.getCurrencyCode(), currencyModel.getCurrencyName(), currencyModel.getCurrencySymbol()};
            StringBuilder sb = new StringBuilder();
            for (String s : s1) {
                sb.append(s).append(",");
            }

            mEditor.putString("C0", sb.toString());
            mEditor.putInt("C01", currencyModel.getFlag());
            mEditor.apply();
            selectedItems.put(currencyModel.getCurrencyCode(), true);
            String jsonString = new Gson().toJson(MainActivity.selectedItems);
            editor.putString("map", jsonString);
            editor.apply();

            CurrencyModel currencyModel1 = new CurrencyModel();
            currencyModel1.setCurrencyName("Canadian dollar");
            currencyModel1.setCurrencyCode("CAD");
            currencyModel1.setFlag(R.drawable.canada);
            currencyModel1.setCurrencySymbol("$");
            tempList.add(currencyModel1);

            String[] s2 = {currencyModel1.getCurrencyCode(), currencyModel1.getCurrencyName(), currencyModel1.getCurrencySymbol()};
            StringBuilder sb1 = new StringBuilder();
            for (int i = 0; i < s1.length; i++) {
                sb1.append(s2[i]).append(",");
            }
            mEditor.putString("C1", sb1.toString());
            mEditor.putInt("C11", currencyModel1.getFlag());
            mEditor.apply();
            selectedItems.put(currencyModel1.getCurrencyCode(), true);
            String jsonString1 = new Gson().toJson(MainActivity.selectedItems);
            editor.putString("map", jsonString1);
            editor.apply();

            CurrencyModel currencyModel2 = new CurrencyModel();
            currencyModel2.setCurrencyName(getResources().getString(R.string._usdollar));
            currencyModel2.setCurrencyCode("USD");
            currencyModel2.setFlag(R.drawable.united_states_of_america);

       /*     mEditor.putString("C2", sb1.toString());
            mEditor.putInt("C21", currencyModel1.getFlag());
            mEditor.apply();*/

            selectedItems.put(currencyModel2.getCurrencyCode(), true);
            String jsonString2 = new Gson().toJson(MainActivity.selectedItems);
            editor.putString("map", jsonString2);
            editor.apply();
        }

        Date currentTime = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        String presentTime = sdf.format(currentTime);
        String previousTime = priceSharedPreferences.getString("currenttime",presentTime);
        timeForUpdation = priceSharedPreferences.getLong("updateTime", 30);

        Date previousDate, presentDate;
        try {
            previousDate = sdf.parse(previousTime);
            presentDate = sdf.parse(presentTime);
            if (presentDate != null && previousDate != null) {
                timeDifference = printDifference(previousDate, presentDate);
            }
            Log.e(TAG, "onCreate: time previousTime" + previousTime + "presentTime" + presentTime);
            Log.e(TAG, "onCreate: time difference" + timeDifference + " minutes");
            Log.e(TAG, "onCreate: time timeForUpdation" + timeForUpdation + " minutes ");

            tvDateTime.setText((getResources().getString(R.string.last_updated)+ " " + previousTime));

            if (timeDifference > timeForUpdation) {
                if (!isFromLanguage) {
                    if (isConnected(activity)) {
                        getJsonData();
                        new DisplayCurrencyValue().execute();
                        Log.e(TAG, "onCreate: DisplayCurrency update time");
                        getTime();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        tvDateTime.setText((getResources().getString(R.string.last_updated) + " " + sdf1.format(Calendar.getInstance().getTime())));
                    } else {
                        Toast.makeText(activity, "Connect to Internet and Refresh", Toast.LENGTH_LONG).show();
                    }
                }
            } /*else {
                tvDateTime.setText((getResources().getString(R.string.last_updated) + " " + previousTime));
            }*/

            if (isFromLanguage) {
                isFromLanguage = false;
                if (isConnected(activity)) {
                    getJsonData();
                    new DisplayCurrencyValue().execute();
                    Log.e(TAG, "onCreate: DisplayCurrency isFromLanguage");
                } else {
                    Toast.makeText(activity, "Connect to Internet and Refresh", Toast.LENGTH_LONG).show();
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "onCreate: " + value);
        if (value) {
            SharedPreferences sharedPreferences1 = getSharedPreferences("HashMap", MODE_PRIVATE);
            String defValue = new Gson().toJson(new SparseBooleanArray());
            String json = sharedPreferences1.getString("map", defValue);
            TypeToken<HashMap<String, Boolean>> token = new TypeToken<HashMap<String, Boolean>>() {};
            selectedItems = new Gson().fromJson(json, token.getType());
        }

        currencyAdapter = new CurrencyAdapter(MainActivity.this, tempList);
        rvCurrency.setAdapter(currencyAdapter);


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    public void findViews() {
//        bottomAppBar = findViewById(R.id.bottom_bar);

        tvCurrencyName = findViewById(R.id.tv_currency_name);
        ivRefresh = findViewById(R.id.ic_refresh);
        ivChart = findViewById(R.id.ic_chart);
        ivPercent = findViewById(R.id.ic_percent);
        ivDots = findViewById(R.id.ic_dots);
        ivCalculator = findViewById(R.id.ic_calculator);
        fabConvert = findViewById(R.id.fab_covert);
        llDisplayCurrency = findViewById(R.id.ll_currency_display);
        llFragmentContainer = findViewById(R.id.ll_fragment_container);
        ivFlag = findViewById(R.id.iv_flag);
        ivManualRefresh = findViewById(R.id.iv_manual_refresh);
        ivCreateAlert = findViewById(R.id.iv_create_alert);
        cvCurrencyDisplay = findViewById(R.id.cv_currency_display);
        llMoreOptions = findViewById(R.id.ll_more_options);

        rvCurrency = findViewById(R.id.rv_currency);
        rvCurrency.setLayoutManager(new GridLayoutManager(this, 2));

        tvDateTime = findViewById(R.id.tv_date_time);
        etCurrencyValue = findViewById(R.id.et_currency_value);
        tvCurrencyCode = findViewById(R.id.tv_currency_code);
        tvCurrencyName = findViewById(R.id.tv_currency_name);
        tvSymbol = findViewById(R.id.tv_symbol);

        tvAppName = findViewById(R.id.tv_app_name);
        tvMoreOptions = findViewById(R.id.tv_more_options);

        fabAdd = findViewById(R.id.fab_add);

        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        llCalculator = findViewById(R.id.linearLayoutCalculator);
        tv0 = findViewById(R.id.tv_0);
        tv1 = findViewById(R.id.tv_1);
        tv2 = findViewById(R.id.tv_2);
        tv3 = findViewById(R.id.tv_3);
        tv4 = findViewById(R.id.tv_4);
        tv5 = findViewById(R.id.tv_5);
        tv6 = findViewById(R.id.tv_6);
        tv7 = findViewById(R.id.tv_7);
        tv8 = findViewById(R.id.tv_8);
        tv9 = findViewById(R.id.tv_9);
        tvDivide = findViewById(R.id.tv_divide);
        tvDot = findViewById(R.id.tv_dot);
        tvDone = findViewById(R.id.tv_done);
        tvMinus = findViewById(R.id.tv_minus);
        tvPlus = findViewById(R.id.tv_plus);
        tvMultiplie = findViewById(R.id.tv_multiple);
        tvDelete = findViewById(R.id.tv_Delete);
        tvClearAll = findViewById(R.id.tv_ClearAll);

        llGraphText = findViewById(R.id.ll_graph_text);

        llGraphsource = findViewById(R.id.ll_graph_source);
        ivGraphSource = findViewById(R.id.iv_graph_source_flag);
        tvGraphSource = findViewById(R.id.tv_graph_source);

        llGraphDestination = findViewById(R.id.ll_graph__destination);
        ivGraphDestination = findViewById(R.id.iv_graph_dest_flag);
        tvGraphDestination = findViewById(R.id.tv_graph_dest);

        cvSwap = findViewById(R.id.cv_swap);

        tvFrom = findViewById(R.id.tv_from);
        tvTo = findViewById(R.id.tv_tooo);
    }

    public void onClicklisters() {
        fabConvert.setOnClickListener(this);
        ivRefresh.setOnClickListener(this);
        ivChart.setOnClickListener(this);
        ivPercent.setOnClickListener(this);
        ivDots.setOnClickListener(this);
        ivCalculator.setOnClickListener(this);
        fabAdd.setOnClickListener(this);
        tv0.setOnClickListener(this);
        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);
        tv3.setOnClickListener(this);
        tv4.setOnClickListener(this);
        tv5.setOnClickListener(this);
        tv6.setOnClickListener(this);
        tv7.setOnClickListener(this);
        tv8.setOnClickListener(this);
        tv9.setOnClickListener(this);
        tvDivide.setOnClickListener(this);
        tvMultiplie.setOnClickListener(this);
        tvMinus.setOnClickListener(this);
        tvPlus.setOnClickListener(this);
        tvDot.setOnClickListener(this);
        tvDone.setOnClickListener(this);
        tvClearAll.setOnClickListener(this);
        tvDelete.setOnClickListener(this);
        ivManualRefresh.setOnClickListener(this);
        llGraphsource.setOnClickListener(this);
        llGraphDestination.setOnClickListener(this);
        ivCreateAlert.setOnClickListener(this);
        cvSwap.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint({"RestrictedApi", "SetTextI18n"})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_covert:
                tvDateTime.setVisibility(View.VISIBLE);
                ivManualRefresh.setVisibility(View.VISIBLE);
                llDisplayCurrency.setVisibility(View.VISIBLE);
                cvCurrencyDisplay.setVisibility(View.VISIBLE);
                rvCurrency.setVisibility(View.VISIBLE);
                llFragmentContainer.setVisibility(View.GONE);
                llGraphText.setVisibility(View.GONE);
                fabAdd.setVisibility(View.VISIBLE);
                llMoreOptions.setVisibility(View.GONE);
                break;

            case R.id.ic_refresh:
             /*   tvDateTime.setVisibility(View.GONE);
                llDisplayCurrency.setVisibility(View.GONE);
                rvCurrency.setVisibility(View.GONE);
                llFragmentContainer.setVisibility(View.VISIBLE);

                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.ll_fragment_container, new SendMoney()).commit();*/

                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.time_interval_dialog);
                final String[] strRadiobtnText = new String[1];

               /* alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                Intent alarmIntent = new Intent(getApplicationContext(), AlarmReceiver.class);
                pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                alarmIntent.setData((Uri.parse("custom://" + System.currentTimeMillis())));
                assert alarmManager != null;
                alarmManager.cancel(pendingIntent);*/

                final RadioButton radioButton30 = dialog.findViewById(R.id.rb_30minutes);
                final RadioButton radioButton1hour = dialog.findViewById(R.id.rb_1hour);
                final RadioButton radioButton2hour = dialog.findViewById(R.id.rb_2hours);
                final RadioButton radioButton3hour = dialog.findViewById(R.id.rb_3hours);
                final RadioButton radioButton4hour = dialog.findViewById(R.id.rb_4hours);
                TextView tvUpdate = dialog.findViewById(R.id.tv_update);

                radioButton30.setChecked(priceSharedPreferences.getBoolean("30minutes", true));
                radioButton1hour.setChecked(priceSharedPreferences.getBoolean("60minutes", false));
                radioButton2hour.setChecked(priceSharedPreferences.getBoolean("120minutes", false));
                radioButton3hour.setChecked(priceSharedPreferences.getBoolean("180minutes", false));
                radioButton4hour.setChecked(priceSharedPreferences.getBoolean("240minutes", false));

                strRadiobtnText[0] = "30 minutes";
                timeForUpdation = 30;
                priceEditor.putLong("updateTime", timeForUpdation);
                priceEditor.apply();

                radioButton30.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        strRadiobtnText[0] = "30 minutes";
                        timeForUpdation = 30;
                        priceEditor.putLong("updateTime", timeForUpdation);
                        priceEditor.apply();
                       /* Calendar alarmStartTime = Calendar.getInstance();
                        Calendar now = Calendar.getInstance();
                        if (now.after(alarmStartTime)) {
                            Log.e("Hey", "Added a day");
                            alarmStartTime.add(Calendar.DATE, 1);
                        }
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmStartTime.getTimeInMillis(), AlarmManager.INTERVAL_HALF_HOUR, pendingIntent);*/
                    }
                });

                radioButton1hour.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        strRadiobtnText[0] = "One hour";
                        timeForUpdation = 60;
                        priceEditor.putLong("updateTime", timeForUpdation);
                        priceEditor.apply();
                     /*   Calendar alarmStartTime = Calendar.getInstance();
                        Calendar now = Calendar.getInstance();
                        if (now.after(alarmStartTime)) {
                            Log.e("Hey", "Added a day");
                            alarmStartTime.add(Calendar.DATE, 1);
                        }
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmStartTime.getTimeInMillis(),
                                AlarmManager.INTERVAL_HOUR, pendingIntent);*/
                    }
                });

                radioButton2hour.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        strRadiobtnText[0] = "Two hour";
                        timeForUpdation = 120;
                        priceEditor.putLong("updateTime", timeForUpdation);
                        priceEditor.apply();
                      /*  Calendar alarmStartTime = Calendar.getInstance();
                        Calendar now = Calendar.getInstance();
                        if (now.after(alarmStartTime)) {
                            Log.e("Hey", "Added a day");
                            alarmStartTime.add(Calendar.DATE, 1);
                        }
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmStartTime.getTimeInMillis(),
                                2 * AlarmManager.INTERVAL_HOUR, pendingIntent);*/
                    }
                });

                radioButton3hour.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        strRadiobtnText[0] = "Three hour";
                        timeForUpdation = 180;
                        priceEditor.putLong("updateTime", timeForUpdation);
                        priceEditor.apply();
                       /* Calendar alarmStartTime = Calendar.getInstance();
                        Calendar now = Calendar.getInstance();
                        if (now.after(alarmStartTime)) {
                            Log.e("Hey", "Added a day");
                            alarmStartTime.add(Calendar.DATE, 1);
                        }
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmStartTime.getTimeInMillis(),
                                3 * AlarmManager.INTERVAL_HOUR, pendingIntent);*/
                    }
                });

                radioButton4hour.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        strRadiobtnText[0] = "Four hour";
                        timeForUpdation = 240;
                        priceEditor.putLong("updateTime", timeForUpdation);
                        priceEditor.apply();
                      /*  Calendar alarmStartTime = Calendar.getInstance();
                        Calendar now = Calendar.getInstance();
                        if (now.after(alarmStartTime)) {
                            Log.e("Hey", "Added a day");
                            alarmStartTime.add(Calendar.DATE, 1);
                        }
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmStartTime.getTimeInMillis(),
                                4 * AlarmManager.INTERVAL_HOUR, pendingIntent);*/
                    }
                });

                tvUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getTime();
                        priceEditor.putBoolean("30minutes", radioButton30.isChecked());
                        priceEditor.putBoolean("60minutes", radioButton1hour.isChecked());
                        priceEditor.putBoolean("120minutes", radioButton2hour.isChecked());
                        priceEditor.putBoolean("180minutes", radioButton3hour.isChecked());
                        priceEditor.putBoolean("240minutes", radioButton4hour.isChecked());
                        priceEditor.apply();

                        Toast.makeText(MainActivity.this, "All Currency rates will be updated every " + strRadiobtnText[0], Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });

                dialog.show();

                break;

            case R.id.iv_create_alert:
                isFromAlert = true;
                isFromChart = false;
                isFromCoversion = false;

                tvDateTime.setVisibility(View.GONE);
                llDisplayCurrency.setVisibility(View.GONE);
                rvCurrency.setVisibility(View.GONE);
                llFragmentContainer.setVisibility(View.VISIBLE);
                ivManualRefresh.setVisibility(View.GONE);
                llGraphText.setVisibility(View.VISIBLE);
                fabAdd.setVisibility(View.GONE);

                FragmentManager manager2 = getSupportFragmentManager();
                FragmentTransaction transaction2 = manager2.beginTransaction();
                transaction2.replace(R.id.ll_fragment_container, new CreateAlertFragment()).commit();
                break;

            case R.id.ic_chart:
                isFromAlert = false;
                isFromChart = true;
                isFromCoversion = false;

                tvDateTime.setVisibility(View.GONE);
                llDisplayCurrency.setVisibility(View.GONE);
                rvCurrency.setVisibility(View.GONE);
                llFragmentContainer.setVisibility(View.VISIBLE);
                ivManualRefresh.setVisibility(View.GONE);
                llGraphText.setVisibility(View.VISIBLE);
                fabAdd.setVisibility(View.GONE);

                FragmentManager manager1 = getSupportFragmentManager();
                Fragment chartfragment = new ChartFragment();

                Bundle data = new Bundle();
                data.putString("source", tvGraphSource.getText().toString());
                data.putString("destination", tvGraphDestination.getText().toString());
                chartfragment.setArguments(data);

                FragmentTransaction transaction1 = manager1.beginTransaction();
                transaction1.replace(R.id.ll_fragment_container, chartfragment).commit();
                break;

            case R.id.ll_graph_source:
                isFromGraphsource = true;
                isFromdest = false;
                Intent intent1 = new Intent(MainActivity.this, AllCurrenciesActivity.class);
                startActivity(intent1);
                break;

            case R.id.ll_graph__destination:
                isFromGraphsource = false;
                isFromdest = true;
                Intent intent2 = new Intent(MainActivity.this, AllCurrenciesActivity.class);
                startActivity(intent2);
                break;

            case R.id.cv_swap:

             /*   String temp = tvGraphSource.getText().toString();
                tvGraphSource.setText(tvGraphDestination.getText().toString());
                tvGraphDestination.setText(temp);*/

                tvGraphSource.setText(mSharedPreferences1.getString("graphDest", "INR"));
                tvGraphDestination.setText(mSharedPreferences1.getString("graphSource", "USD"));

                ivGraphSource.setImageDrawable(getDrawable(mSharedPreferences1.getInt("destFlag", R.drawable.india)));
                ivGraphDestination.setImageDrawable(getDrawable(mSharedPreferences1.getInt("sourceFlag", R.drawable.united_states_of_america)));

                int temp = mSharedPreferences1.getInt("sourceFlag", R.drawable.united_states_of_america);

                mEditor1.putString("graphSource", tvGraphSource.getText().toString());
                mEditor1.putInt("sourceFlag", mSharedPreferences1.getInt("destFlag", R.drawable.india));

                mEditor1.putString("graphDest", tvGraphDestination.getText().toString());
                mEditor1.putInt("destFlag", temp);

                mEditor1.apply();

                if (isFromChart) {
                    FragmentManager manager5 = getSupportFragmentManager();
                    Fragment chartfragment5 = new ChartFragment();
                    FragmentTransaction transaction5 = manager5.beginTransaction();
                    transaction5.replace(R.id.ll_fragment_container, chartfragment5).commit();
                } else if (isFromAlert) {
                    FragmentManager manager5 = getSupportFragmentManager();
                    Fragment chartfragment5 = new CreateAlertFragment();
                    FragmentTransaction transaction5 = manager5.beginTransaction();
                    transaction5.replace(R.id.ll_fragment_container, chartfragment5).commit();
                } else if (isFromCoversion) {
                    FragmentManager manager3 = getSupportFragmentManager();
                    FragmentTransaction transaction3 = manager3.beginTransaction();
                    transaction3.replace(R.id.ll_fragment_container, new CurrencyConversionFragment()).commit();
                } else {
                    FragmentManager manager5 = getSupportFragmentManager();
                    Fragment chartfragment5 = new CreateAlertFragment();
                    FragmentTransaction transaction5 = manager5.beginTransaction();
                    transaction5.replace(R.id.ll_fragment_container, chartfragment5).commit();
                }

                break;

            case R.id.ic_percent:
                isFromAlert = false;
                isFromChart = false;
                isFromCoversion = true;

                tvDateTime.setVisibility(View.GONE);
                ivManualRefresh.setVisibility(View.GONE);
                llDisplayCurrency.setVisibility(View.GONE);
                rvCurrency.setVisibility(View.GONE);
                llGraphText.setVisibility(View.VISIBLE);
                llFragmentContainer.setVisibility(View.VISIBLE);
                fabAdd.setVisibility(View.GONE);

                FragmentManager manager3 = getSupportFragmentManager();
                FragmentTransaction transaction3 = manager3.beginTransaction();
                transaction3.replace(R.id.ll_fragment_container, new CurrencyConversionFragment()).commit();
                break;

            case R.id.ic_dots:
                isFromAlert = false;
                isFromChart = false;
                isFromCoversion = false;

                tvDateTime.setVisibility(View.GONE);
                ivManualRefresh.setVisibility(View.GONE);
                rvCurrency.setVisibility(View.GONE);
                llGraphText.setVisibility(View.GONE);
                llFragmentContainer.setVisibility(View.VISIBLE);
                fabAdd.setVisibility(View.GONE);
                llDisplayCurrency.setVisibility(View.VISIBLE);
                llMoreOptions.setVisibility(View.VISIBLE);
                cvCurrencyDisplay.setVisibility(View.GONE);

                FragmentManager manager4 = getSupportFragmentManager();
                FragmentTransaction transaction4 = manager4.beginTransaction();
                transaction4.replace(R.id.ll_fragment_container, new MoreFragment()).commit();
                break;

            case R.id.fab_add:
                isFromGraphsource = false;
                isFromdest = false;
            /*    SharedPreferences sharedPreferences1 = getSharedPreferences("HashMap", MODE_PRIVATE);
                String defValue = new Gson().toJson(new SparseBooleanArray());
                String json = sharedPreferences1.getString("map", defValue);
                TypeToken<HashMap<String, Boolean>> token = new TypeToken<HashMap<String, Boolean>>() {
                };
                selectedItems = new Gson().fromJson(json, token.getType());*/

                Intent intent = new Intent(MainActivity.this, AllCurrenciesActivity.class);
                startActivity(intent);
                break;

            case R.id.ic_calculator:
                strGetEtValue = etCurrencyValue.getText().toString();
                llFragmentContainer.setVisibility(View.GONE);
                fabAdd.setVisibility(View.GONE);
                coordinatorLayout.setVisibility(View.GONE);
                rvCurrency.setVisibility(View.GONE);
                tvSymbol.setVisibility(View.GONE);
                llCalculator.setVisibility(View.VISIBLE);
                break;

            case R.id.iv_manual_refresh:
                if (isConnected(activity)) {
                    getJsonData();
                    new DisplayCurrencyValue().execute();
                    Log.e(TAG, "onClick: DisplayCurrency Manual refresh");
                    getTime();
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//                    tvDateTime.setText(String.format(" Last updated at %s", sdf1.format(Calendar.getInstance().getTime())));
                    tvDateTime.setText((getResources().getString(R.string.last_updated) + sdf1.format(Calendar.getInstance().getTime())));
                } else {
                    Toast.makeText(activity, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.tv_0:
                if (cal_val_clear) {
                    etCurrencyValue.setText(null);
                    ans += "0";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "0");
                    cal_val_clear = false;
                } else {
                    ans += "0";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "0");
                }

                break;

            case R.id.tv_1:
                if (cal_val_clear) {
                    etCurrencyValue.setText(null);
                    cal_val_clear = false;
                    ans = "1";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "1");
                } else {
                    ans += "1";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "1");
                }

                break;

            case R.id.tv_2:
                if (cal_val_clear) {
                    etCurrencyValue.setText(null);
                    cal_val_clear = false;
                    ans = "2";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "2");
                } else {
                    ans += "2";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "2");
                }

                break;

            case R.id.tv_3:
                if (cal_val_clear) {
                    etCurrencyValue.setText(null);
                    cal_val_clear = false;
                    ans = "3";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "3");
                } else {
                    ans += "3";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "3");
                }
                break;

            case R.id.tv_4:
                if (cal_val_clear) {
                    etCurrencyValue.setText(null);
                    cal_val_clear = false;
                    ans = "4";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "4");
                } else {
                    ans += "4";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "4");
                }
                break;

            case R.id.tv_5:
                if (cal_val_clear) {
                    etCurrencyValue.setText(null);
                    cal_val_clear = false;
                    ans = "5";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "5");
                } else {
                    ans += "5";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "5");
                }
                break;

            case R.id.tv_6:
                if (cal_val_clear) {
                    etCurrencyValue.setText(null);
                    cal_val_clear = false;
                    ans = "6";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "6");
                } else {
                    ans += "6";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "6");
                }
                break;

            case R.id.tv_7:
                if (cal_val_clear) {
                    etCurrencyValue.setText(null);
                    cal_val_clear = false;
                    ans = "7";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "7");
                } else {
                    ans += "7";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "7");
                }
                break;

            case R.id.tv_8:
                if (cal_val_clear) {
                    etCurrencyValue.setText(null);
                    cal_val_clear = false;
                    ans = "8";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "8");
                } else {
                    ans += "8";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "8");
                }
                break;

            case R.id.tv_9:
                if (cal_val_clear) {
                    etCurrencyValue.setText(null);
                    cal_val_clear = false;
                    ans = "9";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "9");
                } else {
                    ans += "9";
                    etCurrencyValue.setText(etCurrencyValue.getText() + "9");
                }
                break;

            case R.id.tv_divide:
                cal_val_clear = false;

                if (!etCurrencyValue.getText().toString().isEmpty()) {
                    String s = etCurrencyValue.getText().toString();
                    char ch = s.charAt(s.length() - 1);
                    if (ch != '+' && ch != '-' && ch != '%' && ch != '*' && ch != '/' && ch != '.') {
                        a.add(s.length() - 1);
                        ans += "/";
                        etCurrencyValue.setText(etCurrencyValue.getText() + "/");
                    }
                }
                break;

            case R.id.tv_multiple:
                cal_val_clear = false;
                if (!etCurrencyValue.getText().toString().isEmpty()) {
                    String s = etCurrencyValue.getText().toString();
                    char ch = s.charAt(s.length() - 1);
                    if (ch != '+' && ch != '-' && ch != '%' && ch != '*' && ch != '/' && ch != '.') {
                        a.add(s.length() - 1);
                        ans += "*";
                        etCurrencyValue.setText(etCurrencyValue.getText() + "*");
                    }
                }

                break;

            case R.id.tv_minus:
                cal_val_clear = false;
                if (!etCurrencyValue.getText().toString().isEmpty()) {
                    String s = etCurrencyValue.getText().toString();
                    char ch = s.charAt(s.length() - 1);
                    if (ch != '+' && ch != '-' && ch != '%' && ch != '*' && ch != '/' && ch != '.') {
                        a.add(s.length() - 1);
                        ans += "-";
                        etCurrencyValue.setText(etCurrencyValue.getText() + "-");
                    }
                }
                break;

            case R.id.tv_plus:
                cal_val_clear = false;
                if (!etCurrencyValue.getText().toString().isEmpty()) {
                    String s1 = etCurrencyValue.getText().toString();
                    char ch = s1.charAt(s1.length() - 1);
                    if (ch != '+' && ch != '-' && ch != '%' && ch != '*' && ch != '/' && ch != '.') {
                        a.add(s1.length() - 1);
                        ans += "+";
                        etCurrencyValue.setText(etCurrencyValue.getText() + "+");
                    }
                }
                break;

            case R.id.tv_dot:
                cal_val_clear = false;
                if (!etCurrencyValue.getText().toString().isEmpty()) {
                    String s2 = etCurrencyValue.getText().toString();
                    char ch = s2.charAt(s2.length() - 1);
                    if (ch != '+' && ch != '-' && ch != '%' && ch != '*' && ch != '/' && ch != '.') {
                        ans += ".";
                        etCurrencyValue.setText(etCurrencyValue.getText() + ".");
                    }

                }
                break;

            case R.id.tv_done:
                tvSymbol.setVisibility(View.VISIBLE);
                caclulation();
                Log.e(TAG, "onClick: ans" + ans);
                if (language.equals("ar")) {
                    ans = arabicToDecimal(ans);
                    ans = ans.replace("٬", "");
                    ans = ans.replace("٫", ".");
                    etCurrencyValue.setText(arabicToDecimal(ans));
                } else {
                    etCurrencyValue.setText(ans);
                }

                 /* allCode = new String[tempList.size()];
                Log.e(TAG, "onResume: " + tempList.size());
                for (int i = 0; i < tempList.size(); i++) {
                    allCode[i] = tempList.get(i).getCurrencyCode();
                }
                StringBuilder sb = new StringBuilder();
                for (String datum : allCode) {
                    sb.append(datum).append(",");
                }
                strAllcode = sb.toString();

                strSource = tvCurrencyCode.getText().toString();
                strCurrencies = strAllcode;
                Log.e(TAG, "onResume: strAllcode" + strAllcode);

                new DisplayCurrencyValue().execute();

                currencyAdapter = new CurrencyAdapter(MainActivity.this, tempList);
                rvCurrency.setAdapter(currencyAdapter);*/

                SharedPreferences priceSharedPreferences = getSharedPreferences("allPriceList", MODE_PRIVATE);
                try {
                    HashMap<String, String> tempPriceList = (HashMap<String, String>) ObjectSerializer.deserialize(priceSharedPreferences.getString("myPriceList", ObjectSerializer.serialize(new HashMap<String, String>())));
                    assert tempPriceList != null;
                    Log.e(TAG, "onDone: temPriceList" + tempPriceList.size());
                    for (int i = 0; i < tempList.size(); i++) {
                        Log.e(TAG, "onDone: size" + tempList.size());
                        for (Map.Entry<String, String> entry : tempPriceList.entrySet()) {
                            String key = entry.getKey();
                            String value = entry.getValue();
                            Log.e(TAG, "onDone" + "key " + key + "value " + value);
                            if (tempList.get(i).getCurrencyCode().equals(entry.getKey())) {
                                Log.e(TAG, "onDone: match found ");
                                if (language.equals("ar")) {
                                    ans = arabicToDecimal(ans);
                                    ans = ans.replace("٬", "");
                                    ans = ans.replace("٫", ".");

                                    strGetEtValue = arabicToDecimal(strGetEtValue);
                                    strGetEtValue = strGetEtValue.replace("٬", "");
                                    strGetEtValue = strGetEtValue.replace("٫", ".");
                                    tempList.get(i).setCurrencyPrice((tempList.get(i).getCurrencyPrice() * Double.parseDouble(ans)) / Double.parseDouble(strGetEtValue));
                                } else {
                                    tempList.get(i).setCurrencyPrice((tempList.get(i).getCurrencyPrice() * Double.parseDouble(ans)) / Double.parseDouble(strGetEtValue));
                                    Log.e(TAG, "onClick: !1-->" + Double.parseDouble(value));
                                    Log.e(TAG, "onClick: !2-->" + Double.parseDouble(ans));
                                    Log.e(TAG, "onClick: !3-->" + Double.parseDouble(strGetEtValue));
                                }
                                currencyAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }

                break;

            case R.id.tv_ClearAll:
                ans = "";
                etCurrencyValue.setText(null);
                a.clear();
                break;

            case R.id.tv_Delete:

                if (!etCurrencyValue.getText().toString().isEmpty()) {
                    String s3 = etCurrencyValue.getText().toString();
                    StringBuilder s1 = new StringBuilder();
                    for (int i = 0; i < s3.length() - 1; i++) {
                        s1.append(s3.charAt(i));
                    }
                    ans = s1.toString();
                    etCurrencyValue.setText(s1.toString());
                }
                break;

        }
    }

    @SuppressLint("RestrictedApi")
    private void caclulation() {

        cal_val_clear = true;
        Log.e("asdfg", "caclulation: " + etCurrencyValue.getText().toString());
        if (!etCurrencyValue.getText().toString().isEmpty()) {
            String s = ans;
            char ch = s.charAt(s.length() - 1);
            if (ch == '+' || ch == '-' || ch == '%' || ch == '*' || ch == '/' || ch == '.') {
                Toast.makeText(getApplicationContext(), "Invalid", Toast.LENGTH_SHORT).show();
                cal_val_clear = false;
            } else {
                a.add(s.length() - 1);
                s += "=";
                //String info="";

                //boolean div=false,mult=false,sub=false,perc=false,add1=false;
                if (!s.substring(0, a.get(0) + 1).isEmpty()) {
                    double res;
                    if (language.equals("ar")) {
                        res = Double.parseDouble(arabicToDecimal(s.substring(0, a.get(0) + 1)));
                    } else {
                        res = Double.parseDouble(s.substring(0, a.get(0) + 1));
                    }
//                double res = Double.parseDouble(s.substring(0, a.get(0) + 1));

                    for (int i = 0; i < a.size() - 1; i++) {
                        double answ = Double.parseDouble(s.substring(a.get(i) + 2, a.get(i + 1) + 1));
//                        double answ = xyz;
//                    double answ = Double.parseDouble(s.substring(a.get(i) + 2, a.get(i + 1) + 1));
                        if (s.charAt(a.get(i) + 1) == '+') {
                            res += answ;
                        } else if (s.charAt(a.get(i) + 1) == '-') {
                            res -= answ;
                        } else if (s.charAt(a.get(i) + 1) == '/') {
                            res /= answ;
                        } else if (s.charAt(a.get(i) + 1) == '*') {
                            res *= answ;
                        }
                    }
                    String input;
                    Log.e("abcd", "caclulation: res " + res);
                    if (language.equals("ar")) {
                        NumberFormat nf = NumberFormat.getInstance(new Locale("ar", "EG"));
                        nf.setMaximumFractionDigits(2);
                        input = nf.format(res);
                    } else {
                        input = (new DecimalFormat("##.##").format(res));
                    }
                    Log.e("abcd", "caclulation: ans " + input);
                    Log.e("abcd", "caclulation: prev ans " + prev_ans);

                    if (input.equals("0")) {
                        input = "1";
                    }

                    if (res > -1) {
                        prev_ans = input;
                        Log.e("abcd", "caclulation: if ans " + input);
                        Log.e("abcd", "caclulation: if prev ans " + prev_ans);

                    } else {
                        Toast.makeText(this, "Invalid Amount", Toast.LENGTH_SHORT).show();
                        input = prev_ans;
                    }

                    a.clear();
                    if (language.equals("ar")) {
                        etCurrencyValue.setText(arabicCurrencyFormatter(input));
//                       etCurrencyValue.setText(arabicToDecimal(input));
                    } else {
                        etCurrencyValue.setText(currencyFormatter(input));
                    }
                    ans = input;

                    llCalculator.setVisibility(View.GONE);
//                    llFragmentContainer.setVisibility(View.VISIBLE);
                    fabAdd.setVisibility(View.VISIBLE);
                    coordinatorLayout.setVisibility(View.VISIBLE);
                    rvCurrency.setVisibility(View.VISIBLE);
                }
            }
        } else {
            String input = String.valueOf(1);
            a.clear();
            if (language.equals("ar")) {
                etCurrencyValue.setText(arabicCurrencyFormatter(input));
//                etCurrencyValue.setText(arabicToDecimal(input));
            } else {
                etCurrencyValue.setText(currencyFormatter(input));
            }
            //a.add(input.length()-1);
            ans = input;
            tvSymbol.setVisibility(View.VISIBLE);
            llCalculator.setVisibility(View.GONE);
            llFragmentContainer.setVisibility(View.GONE);
            fabAdd.setVisibility(View.VISIBLE);
            coordinatorLayout.setVisibility(View.VISIBLE);
            rvCurrency.setVisibility(View.VISIBLE);
        }
    }

    public String currencyFormatter(String num) {
        double m = Double.parseDouble(num);
        DecimalFormat formatter = new DecimalFormat("###,###,###.##");
        return formatter.format(m);
    }

    public String arabicCurrencyFormatter(String num) {
        num = arabicToDecimal(num);
        num = num.replace("٬", "");
        num = num.replace("٫", ".");
        Double d = Double.parseDouble(num);
        NumberFormat nf = NumberFormat.getInstance(new Locale("ar", "EG"));
        nf.setMaximumFractionDigits(4);
        return nf.format(d);
    }

    private static String arabicToDecimal(String number) {
        char[] chars = new char[number.length()];
        for (int i = 0; i < number.length(); i++) {
            char ch = number.charAt(i);
            if (ch >= 0x0660 && ch <= 0x0669)
                ch -= 0x0660 - '0';
            else if (ch >= 0x06f0 && ch <= 0x06F9)
                ch -= 0x06f0 - '0';
            chars[i] = ch;
        }
        Log.e(TAG, "arabicToDecimal: " + new String(chars));
        return new String(chars);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onResume() {
        super.onResume();

        Log.e(TAG, "cycle onResume: ");

        SharedPreferences sharedPreferences = getSharedPreferences("clickCount", MODE_PRIVATE);
        SharedPreferences sharedPreferences1 = getSharedPreferences("convert", MODE_PRIVATE);
        value = sharedPreferences.getBoolean("isClicked", false);

        tvGraphSource.setText(sharedPreferences1.getString("graphSource", "USD"));
        tvGraphDestination.setText(sharedPreferences1.getString("graphDest", "INR"));

        ivGraphSource.setImageDrawable(getDrawable(sharedPreferences1.getInt("sourceFlag", R.drawable.united_states_of_america)));
        ivGraphDestination.setImageDrawable(getDrawable(sharedPreferences1.getInt("destFlag", R.drawable.india)));

        if (!value) {
            Log.e(TAG, "onResume: in value");
            tvCurrencyName.setText(getResources().getString(R.string._usdollar));
            tempList.clear();
            tempList = new ArrayList<>();
            addModelToList(sharedPreferences, "C0", "C01");
            addModelToList(sharedPreferences, "C1", "C11");
        }

        if (value) {

            Log.e(TAG, "onResume: in value");
            tempList.clear();
            tempList = new ArrayList<>();

            if (sharedPreferences.contains("C0")) {
                addModelToList(sharedPreferences, "C0", "C01");
            }

            if (sharedPreferences.contains("C1")) {
                addModelToList(sharedPreferences, "C1", "C11");
            }

            if (sharedPreferences.contains("C2")) {
                addModelToList(sharedPreferences, "C2", "C21");
            }

            if (sharedPreferences.contains("C3")) {
                addModelToList(sharedPreferences, "C3", "C31");
            }

            if (sharedPreferences.contains("C4")) {
                addModelToList(sharedPreferences, "C4", "C41");
            }

            if (sharedPreferences.contains("C5")) {
                addModelToList(sharedPreferences, "C5", "C51");
            }

            if (sharedPreferences.contains("C6")) {
                addModelToList(sharedPreferences, "C6", "C61");
            }

            if (sharedPreferences.contains("C7")) {
                addModelToList(sharedPreferences, "C7", "C71");
            }

            if (sharedPreferences.contains("C8")) {
                addModelToList(sharedPreferences, "C8", "C81");
            }
            if (sharedPreferences.contains("C9")) {
                addModelToList(sharedPreferences, "C9", "C91");
            }
        }

        /*allCode = new String[tempList.size()];
        Log.e(TAG, "onResume: " + tempList.size());
        for (int i = 0; i < tempList.size(); i++) {
            allCode[i] = tempList.get(i).getCurrencyCode();
        }
        StringBuilder sb = new StringBuilder();
        for (String datum : allCode) {
            sb.append(datum).append(",");
        }
        strAllcode = sb.toString();

        strSource = tvCurrencyCode.getText().toString();
        strCurrencies = strAllcode;
        Log.e(TAG, "onResume: strAllcode" + strAllcode);

        new DisplayCurrencyValue().execute();*/

        SharedPreferences priceSharedPreferencesList = getSharedPreferences("allPriceList", MODE_PRIVATE);
        try {
            HashMap<String, String> tempPriceList = (HashMap<String, String>) ObjectSerializer.deserialize(priceSharedPreferencesList.getString("myPriceList", ObjectSerializer.serialize(new HashMap<String, String>())));
            assert tempPriceList != null;
            Log.e(TAG, "onResume: temPriceList" + tempPriceList.size());
            for (int i = 0; i < tempList.size(); i++) {
                Log.e(TAG, "onResume: size" + tempList.size());
                for (Map.Entry<String, String> entry : tempPriceList.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();

                    if (tvCurrencyCode.getText().toString().equals(entry.getKey())) {
                        String strEntryValue = entry.getValue();
                        Log.e(TAG, "onResume: strvalue" + strEntryValue);
                        String strEtValue = etCurrencyValue.getText().toString();
                        Log.e(TAG, "onResume: " + strEtValue);
                        dbValueToMultiply = (Double.parseDouble(strEtValue)) / (Double.parseDouble(strEntryValue));
                    }

                    Log.e(TAG, "onResume" + "key " + key + " value " + value);
                    if (tempList.get(i).getCurrencyCode().equals(entry.getKey())) {
                        tempList.get(i).setCurrencyPrice((Double.parseDouble(value) * dbValueToMultiply));
                        if (tvCurrencyCode.getText().toString().equals(entry.getKey())) {
                            Log.e(TAG, "onResume: !1" + value);
                            Log.e(TAG, "onResume: !2" + entry.getValue());
                            Log.e(TAG, "onResume: !3" + dbGetValue);
                            dbGetValue = Double.parseDouble(value) / Double.parseDouble(entry.getValue());
                        }
                        dbGetValue = Double.parseDouble(etCurrencyValue.getText().toString()) / tempList.get(i).getCurrencyPrice();
                        Log.e(TAG, "onResume:ar dbGetValue" + dbGetValue);
                        Log.e(TAG, "onResume:ar et value" + etCurrencyValue.getText().toString());
                        Log.e(TAG, "onResume:ar temp value" + tempList.get(i).getCurrencyPrice());
                        tempList.get(i).setCurrencyValue(("1" + tempList.get(i).getCurrencyCode() + " = " + String.format(Locale.getDefault(), "%.5f", dbGetValue) + " " + tvCurrencyCode.getText().toString()));
                        currencyAdapter.notifyDataSetChanged();
                    }
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        currencyAdapter = new CurrencyAdapter(MainActivity.this, tempList);
        rvCurrency.setAdapter(currencyAdapter);
    }

    private void addModelToList(SharedPreferences sharedPreferences, String c12, String c11) {
        CurrencyModel model = new CurrencyModel();
        String c1 = sharedPreferences.getString(c12, "default");
        String[] model1 = c1.split(",");
        model.setCurrencyCode(model1[0]);
        model.setCurrencyName(model1[1]);
        model.setCurrencySymbol(model1[2]);
        model.setFlag(sharedPreferences.getInt(c11, -1));
        tempList.add(model);
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "cycle onPause: ");

        priceEditor.putString("etValue", etCurrencyValue.getText().toString());
        priceEditor.apply();

    }

    private String loadJSON() {
        String json;
        try {
            InputStream is;
            if (language.equals("ar")) {
                is = getAssets().open("arabiccurrency");
            } else {
                is = getAssets().open("currencydetails");
            }
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
            Log.e(TAG, "loadJSON: response" + json);
        } catch (IOException ex) {
            ex.printStackTrace();
            Log.e(TAG, "loadJSON: catch" + ex.getMessage());
            return null;
        }
        return json;
    }

    private void getJsonData() {
        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(loadJSON());
            Log.e(TAG, "getJsonData: " + jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String currencyCode = jsonObject.getString("code");
                currencyCodeslist.add(currencyCode);
                if (currencyCode.equals(tvCurrencyCode.getText().toString())) {
                    editorSwap.putString("swapName", jsonObject.getString("name"));
                    editorSwap.apply();
                    tvCurrencyName.setText(editorSharedPreferences.getString("swapName", "default name"));
                }
                Log.e(TAG, "getJsonData: " + currencyCodeslist.get(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class DisplayCurrencyValue extends AsyncTask<String, String, String> {

        StringBuffer response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Processing..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String urlParameters = "&source=" + "USD" + "&currencies=" + "";
            String baseUrl = "https://apilayer.net/api/live?access_key=86da42ddd38aae4c13bb5b5d44f0f437";
            String url = baseUrl + urlParameters;
            Log.e(TAG, "doInBackground: " + baseUrl);
            URL obj = null;
            try {
                obj = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection con;
            try {
                assert obj != null;
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.e(TAG, "onPostExecute: response" + s);

            progressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("quotes");

                Log.e(TAG, "onPostExecute: size" + currencyCodeslist.size());
                for (int j = 0; j < currencyCodeslist.size(); j++) {
                    allPrices.put(currencyCodeslist.get(j), jsonObject1.getString("USD" + currencyCodeslist.get(j)));
                    Log.e(TAG, "onPostExecute: ssup" + currencyCodeslist.get(j) + "-->" + jsonObject1.getString("USD" + currencyCodeslist.get(j)));
                }

                // TODO: 03-12-2019 save arraylist to sharedpref using objectserializer
                SharedPreferences sharedPreferences = getSharedPreferences("allPriceList", MODE_PRIVATE);
                SharedPreferences.Editor priceEditor = sharedPreferences.edit();
                try {
                    priceEditor.putString("myPriceList", ObjectSerializer.serialize(allPrices));
                    priceEditor.apply();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                for (Map.Entry<String, String> entry : allPrices.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    Log.e(TAG, "onPostExecute: " + "key " + key + " value " + value);
                }
                Log.e(TAG, "onPostExecute: size" + allPrices.size());

                for (int i = 0; i < tempList.size(); i++) {
                    Log.e(TAG, "onResume: size" + tempList.size());
                    for (Map.Entry<String, String> entry : allPrices.entrySet()) {
                        String key = entry.getKey();
                        String value = entry.getValue();

                        String strValue="1";
                        if (tvCurrencyCode.getText().toString().equals(entry.getKey())) {
                            strValue = entry.getValue();
                            Log.e(TAG, "onResume: strvalue" + strValue);
                            String strEtValue = etCurrencyValue.getText().toString();
                            Log.e(TAG, "onResume: " + strEtValue);
                            dbValueToMultiply = (Double.parseDouble(strEtValue)) / (Double.parseDouble(strValue));
                        }

                        double dbGetValue;
                        Log.e(TAG, "onResume" + "key " + key + " value " + value);
                        if (tempList.get(i).getCurrencyCode().equals(entry.getKey())) {
                            Log.e(TAG, "onResume: !1" + ans);
                            Log.e(TAG, "onResume: !2" + value);
//                            dbGetValue = Double.parseDouble(strValue) / Double.parseDouble(value);
                            tempList.get(i).setCurrencyPrice((Double.parseDouble(value) * dbValueToMultiply));

                            dbGetValue = Double.parseDouble(etCurrencyValue.getText().toString()) / tempList.get(i).getCurrencyPrice();
                            tempList.get(i).setCurrencyValue(("1" + tempList.get(i).getCurrencyCode() + " = " + String.format(Locale.getDefault(), "%.5f", dbGetValue) + " " + tvCurrencyCode.getText().toString()));
                            currencyAdapter.notifyDataSetChanged();
                        }
                    }
                }
                currencyAdapter = new CurrencyAdapter(MainActivity.this, tempList);
                rvCurrency.setAdapter(currencyAdapter);

               /* for (int i = 0; i < tempList.size(); i++) {
                    Log.e(TAG, "onPostExecute: ans" + jsonObject1.getString(strSource + tempList.get(i).getCurrencyCode()));
                    tempList.get(i).setCurrencyPrice((Double.parseDouble(jsonObject1.getString(strSource + tempList.get(i).getCurrencyCode()))) * (Double.parseDouble(etCurrencyValue.getText().toString())));
                }
                currencyAdapter.notifyDataSetChanged();*/

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void getTime() {
        Date currentTime = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        priceEditor.putString("currenttime", sdf.format(currentTime));
        priceEditor.apply();
    }

    public long printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long diffInSec = TimeUnit.MILLISECONDS.toMinutes(different);
        Log.e(TAG, "printDifference: diffInMinutes" + (diffInSec));

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        Log.e(TAG, "printDifference: " + elapsedDays + "d " + elapsedHours + "h " + elapsedMinutes + "m " + elapsedSeconds + "s ");
        return diffInSec;
    }

    private void setLocale(String language) {
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration config = new Configuration();
        config.setLocale(new Locale(language));
        res.updateConfiguration(config, dm);
     /*   editorLang.putString("mylanguage",language);
        editorLang.apply();*/
    }

    private boolean isConnected(Context context) {
        boolean connected;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        //we are connected to a network
        assert connectivityManager != null;
        connected = Objects.requireNonNull(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)).getState() == NetworkInfo.State.CONNECTED ||
                Objects.requireNonNull(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)).getState() == NetworkInfo.State.CONNECTED;
        return connected;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        if (llCalculator.isShown()) {
            llCalculator.setVisibility(View.GONE);
            llFragmentContainer.setVisibility(View.VISIBLE);
            fabAdd.setVisibility(View.VISIBLE);
            coordinatorLayout.setVisibility(View.VISIBLE);
            rvCurrency.setVisibility(View.VISIBLE);
        } else if (llFragmentContainer.isShown()) {
            tvDateTime.setVisibility(View.VISIBLE);
            ivManualRefresh.setVisibility(View.VISIBLE);
            llDisplayCurrency.setVisibility(View.VISIBLE);
            cvCurrencyDisplay.setVisibility(View.VISIBLE);
            llMoreOptions.setVisibility(View.GONE);
            rvCurrency.setVisibility(View.VISIBLE);
            llFragmentContainer.setVisibility(View.GONE);
            llGraphText.setVisibility(View.GONE);
            fabAdd.setVisibility(View.VISIBLE);
        } else {
            finish();
        }
    }
}