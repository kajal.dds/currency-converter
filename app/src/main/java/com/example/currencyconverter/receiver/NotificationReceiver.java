package com.example.currencyconverter.receiver;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.example.currencyconverter.R;
import com.example.currencyconverter.activity.MainActivity;
import com.example.currencyconverter.model.CreateAlertModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import static android.app.PendingIntent.FLAG_ONE_SHOT;
import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationReceiver extends BroadcastReceiver {

    private static int NOTIFICATION_ID = 1;
    private Notification notification;
    private static final String TAG = "NotificationReceiver";

    @Override
    public void onReceive(final Context context, Intent intent) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("clickCount", MODE_PRIVATE);
        SharedPreferences.Editor mEditor = sharedPreferences.edit();
        mEditor.apply();


        Gson gson = new Gson();
        String json = sharedPreferences.getString("list", "");
        Type type = new TypeToken<ArrayList<CreateAlertModel>>() {}.getType();
        final ArrayList<CreateAlertModel> arrayList = gson.fromJson(json, type);


        if(arrayList!=null){
            for (int i = 0; i < arrayList.size(); i++) {

                Log.e(TAG, "onCreateView: arraylist" + arrayList.get(i).getWhen().substring(2, 5));
                Log.e(TAG, "onCreateView: arraylist" + arrayList.get(i).getEquals());
                Log.e(TAG, "onCreateView: arraylist" + arrayList.get(i).getAmount());

                Handler handler = new Handler(Looper.getMainLooper());
                final int finalI = i;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(isConnected(context)){
                            new DisplayCurrencyValue(arrayList.get(finalI).getWhen().substring(2, 5), arrayList.get(finalI).getEquals(), arrayList.get(finalI).getAmount(),context).execute();
                        }
                    }
                });
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    class DisplayCurrencyValue extends AsyncTask<String, String, String> {

        StringBuffer response;
        String source, destination;
        double amountt;
        Context context;
        Double result;

        DisplayCurrencyValue(String from, String to, double amount, Context context) {
            this.source = from;
            this.destination = to;
            this.amountt = amount;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            String urlParameters = "&source=" + source + "&currencies=" + destination;
            String baseUrl = "https://apilayer.net/api/live?access_key=86da42ddd38aae4c13bb5b5d44f0f437";
            String url = baseUrl + urlParameters;
            URL obj = null;
            try {
                obj = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection con;
            try {
                assert obj != null;
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("quotes");
                 result = Double.parseDouble(jsonObject1.getString(source + destination));

                Log.e(TAG, "onPostExecute: amount" + amountt);
                Log.e(TAG, "onPostExecute: result" + result);

                if (result>= amountt || result <= amountt) {
                    Log.e(TAG, "onPostExecute: yay alert user now");
                    setNotification(context, source, destination, result);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setNotification(Context context, String source, String destination, double amountt) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        Resources res = context.getResources();
        if (notificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel("channel id", "notification channel", NotificationManager.IMPORTANCE_DEFAULT);
                notificationChannel.enableLights(true);
                notificationChannel.enableVibration(true);
                notificationChannel.setLightColor(Color.GREEN);
                notificationChannel.setVibrationPattern(new long[]{
                        500,
                        500,
                        500,
                        500,
                        500
                });
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

                notificationManager.createNotificationChannel(notificationChannel);

                Log.d(TAG, "onReceive: " + notificationManager.getNotificationChannel("channel_id"));
            }


            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "channel id");

            Intent myIntent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, myIntent, FLAG_ONE_SHOT);

            builder.setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    //replace app icon
                    .setSmallIcon(R.drawable.appicon)
                    .setContentTitle("Currency Converter")
                    //ic replace camera icon
//                    .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.appicon))
                    .setContentIntent(pendingIntent)
                    .setContentText("Current rate: 1 " + source + " = " + amountt + " " + destination)
                    .setSubText("Rate Alert")
                    .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND);

            notificationManager.notify(NOTIFICATION_ID, builder.build());
            NOTIFICATION_ID++;
        }
    }

    private boolean isConnected(Context context) {
        boolean connected;
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
        return connected;
    }
}
