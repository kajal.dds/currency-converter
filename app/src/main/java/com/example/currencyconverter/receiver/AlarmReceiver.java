package com.example.currencyconverter.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Calendar;

public class AlarmReceiver extends BroadcastReceiver {

    SharedPreferences sharedPreferences1;

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.e("TAG", "alert onReceive: in receiver");
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            startAlarm(context);
        } else {
            startAlarm(context);
        }

     /*   Intent service1 = new Intent(context, NotificationService.class);
        service1.setData((Uri.parse("custom://" + System.currentTimeMillis())));
        context.startService(service1);*/

    }

    private void startAlarm(Context context) {

        sharedPreferences1 = context.getSharedPreferences("priceValue", Context.MODE_PRIVATE);

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent myIntent;
        PendingIntent pendingIntent;

        //THIS IS WHERE YOU SET NOTIFICATION TIME FOR CASES WHEN THE NOTIFICATION NEEDS TO BE RESCHEDULED

        Calendar alarmStartTime = Calendar.getInstance();

        myIntent = new Intent(context, NotificationReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(context, 0, myIntent, 0);

        assert manager != null;
        manager.setRepeating(AlarmManager.RTC_WAKEUP, alarmStartTime.getTimeInMillis(),
                sharedPreferences1.getLong("updateTime",30)*60*1000, pendingIntent);
    }

}
