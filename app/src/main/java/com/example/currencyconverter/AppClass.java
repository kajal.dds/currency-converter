package com.example.currencyconverter;

import android.app.Application;
import android.content.SharedPreferences;

import com.onesignal.OneSignal;

public class AppClass extends Application {

    public static int count ;
    private static AppClass appInstance;

    public AppClass() {
        appInstance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        SharedPreferences mSharedPref = getSharedPreferences("appCount",MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPref.edit();

        count = mSharedPref.getInt("counter",0);

        count++;

        mEditor.putInt("counter",count);
        mEditor.apply();


        // TODO: 18-12-2019  One signal notification
        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

    }

    public static synchronized AppClass getInstance(){
        return appInstance;
    }


}
