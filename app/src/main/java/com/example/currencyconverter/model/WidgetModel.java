package com.example.currencyconverter.model;

public class WidgetModel {

    private String widgetSource;
    private String widgetDest;
    private String result;
    private int widgetId;
    private String key;
    private Integer srcFlag;
    private Integer destFlag;

    public Integer getSrcFlag() {
        return srcFlag;
    }

    public void setSrcFlag(Integer srcFlag) {
        this.srcFlag = srcFlag;
    }

    public Integer getDestFlag() {
        return destFlag;
    }

    public void setDestFlag(Integer destFlag) {
        this.destFlag = destFlag;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getWidgetSource() {
        return widgetSource;
    }

    public void setWidgetSource(String widgetSource) {
        this.widgetSource = widgetSource;
    }

    public String getWidgetDest() {
        return widgetDest;
    }

    public void setWidgetDest(String widgetDest) {
        this.widgetDest = widgetDest;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getWidgetId() {
        return widgetId;
    }

    public void setWidgetId(int widgetId) {
        this.widgetId = widgetId;
    }
}
