package com.example.currencyconverter.model;

import java.util.ArrayList;

public class MainModel {

    private String letter;
    private ArrayList<CurrencyModel> currencyModelArrayList;

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public ArrayList<CurrencyModel> getCurrencyModelArrayList() {
        return currencyModelArrayList;
    }

    public void setCurrencyModelArrayList(ArrayList<CurrencyModel> currencyModelArrayList) {
        this.currencyModelArrayList = currencyModelArrayList;
    }
}
