package com.example.currencyconverter.widget;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.example.currencyconverter.R;
import com.example.currencyconverter.activity.MainActivity;
import com.example.currencyconverter.model.WidgetModel;
import com.example.currencyconverter.service.WidgetService1;
import com.example.currencyconverter.service.WidgetServicePie1;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.example.currencyconverter.widget.WidgetSourceActivity.widgetModelList;

/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link WidgetSourceActivity ConfigurableWidgetConfigureActivity}
 */
public class WidgetReceiver extends AppWidgetProvider {

    private static final String TAG = "WidgetReceiver";
    private RemoteViews view;
    private boolean isFromSync;
    private ArrayList<WidgetModel> arrayList;
    private SharedPreferences sharedPreferences1;
    SharedPreferences.Editor editor1, editor2;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        Log.e(TAG, "onUpdate: in widget receiver");
        Log.e(TAG, "onUpdate: sync" + isFromSync);

        if (!isFromSync) {

            Log.e(TAG, "onUpdate: in not sync");

            SharedPreferences sharedPreferences = context.getSharedPreferences("priceValue", Context.MODE_PRIVATE);

            sharedPreferences1 = context.getSharedPreferences("widget", MODE_PRIVATE);
            editor1 = sharedPreferences1.edit();
            editor1.apply();

            editor2 = sharedPreferences1.edit();

            List<Integer> id = new ArrayList<>();
            for (int appWidgetId : appWidgetIds) {
                id.add(appWidgetId);
            }

            Gson gson1 = new Gson();
            String json1 = gson1.toJson(id);
            editor2.putString("widgetIdList", json1);
            editor2.apply();

            view = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
            final AppWidgetManager widgetManager = AppWidgetManager.getInstance(context);

            int[] allids = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, WidgetReceiver.class));

            Gson gson3 = new Gson();
            String json3 = sharedPreferences1.getString("list", "");
            Type type3 = new TypeToken<ArrayList<WidgetModel>>() {}.getType();
            arrayList = gson3.fromJson(json3, type3);

            for (final int allid : allids) {
                Log.e(TAG, "onEnabled: " + allid);
                if (arrayList != null) {
                    Log.e(TAG, "onEnabled: " + arrayList.size());
                    for (int i = 0; i < arrayList.size(); i++) {
                        if (allid == arrayList.get(i).getWidgetId()) {
                            Log.e(TAG, "onEnabled: " + allid + " " + arrayList.get(i).getWidgetId() + " " + arrayList.get(i).getKey() + " " + arrayList.get(i).getResult());

                            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            view.setTextViewText(R.id.tv_widget_date, String.format("%s", sdf1.format(Calendar.getInstance().getTime())));

                            view.setImageViewResource(R.id.iv_widget_src, arrayList.get(i).getSrcFlag());
                            view.setTextViewText(R.id.tv_widget_source, arrayList.get(i).getWidgetSource());

                            view.setImageViewResource(R.id.iv_widget_dest, arrayList.get(i).getDestFlag());
                            view.setTextViewText(R.id.tv_widget_dest, arrayList.get(i).getWidgetDest());

                            view.setTextViewText(R.id.tv_widget_amount, arrayList.get(i).getResult());

                            Intent intent = new Intent(context, MainActivity.class);
                            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allid);
                            Uri data = Uri.withAppendedPath(Uri.parse("URI_SCHEME" + "://widget/id/"), String.valueOf(allid));
                            intent.setData(data);
                            PendingIntent pending = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                            view.setOnClickPendingIntent(R.id.cl_widget_layout, pending);

                            Log.e(TAG, "onClick: mAppWidgetId" + allid);

                            Intent intent1 = new Intent(context, WidgetReceiver.class);
                            intent1.setAction("automaticWidgetSyncButtonClick");
                            intent1.putExtra("widgetId", allid);
                            PendingIntent pending1 = PendingIntent.getBroadcast(context, allid, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                            view.setOnClickPendingIntent(R.id.refresh_widget, pending1);

                            widgetManager.updateAppWidget(allid, view);
                        }
                    }
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Log.e(TAG, "onUpdate: widgetModelList.size() " + widgetModelList.size());

            //////////////////////////////////////////////////////////////////////////////////////////////////////

            ComponentName thisWidget = new ComponentName(context, WidgetReceiver.class);
            int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                ComponentName componentName = new ComponentName(context, WidgetServicePie1.class);
                JobInfo info = new JobInfo.Builder(123, componentName)
                        .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                        .setOverrideDeadline((sharedPreferences.getLong("updateTime", 30)) * 60 * 1000).build();

                JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
                if (scheduler != null) {
                    scheduler.schedule(info);
                }
            } else {
                Intent intent = new Intent(context, WidgetService1.class);
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);
                context.startService(intent);

                final AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                assert manager != null;
                manager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(),
                        (sharedPreferences.getLong("updateTime", 30)) * 60 * 1000, pendingIntent);
                Log.e(TAG, "onUpdate: widget time " + (sharedPreferences.getLong("updateTime", 30)) * 60 * 1000);
            }
        } else {
            isFromSync = false;
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        Log.e(TAG, "onReceive: " + intent.getAction());

        String SYNC_CLICKED = "automaticWidgetSyncButtonClick";
        if (SYNC_CLICKED.equals(intent.getAction())) {
            isFromSync = true;
            sharedPreferences1 = context.getSharedPreferences("widget", MODE_PRIVATE);
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

            Gson gson = new Gson();
            String json = sharedPreferences1.getString("list", "");
            Type type = new TypeToken<ArrayList<WidgetModel>>() {}.getType();
            arrayList = gson.fromJson(json, type);

            if(arrayList!=null && arrayList.size()>0){

                for (int i = 0; i < arrayList.size(); i++) {
                    Log.e(TAG, "onReceive: " + arrayList.get(i).getWidgetId() + " " + arrayList.get(i).getKey() + arrayList.get(i).getResult());
                }

                for (int i = 0; i < widgetModelList.size() ; i++) {
                    Log.e(TAG, "onReceive: w"+ widgetModelList.get(i).getWidgetId() );
                }

                int idd = intent.getExtras().getInt("widgetId");
                Log.e(TAG, "onReceive: " + idd);

                Log.e(TAG, "onReceive: ");
                for (int i = 0; i < arrayList.size(); i++) {
                    Log.e(TAG, "onReceive: in" + arrayList.get(i).getWidgetId() + arrayList.get(i).getKey());
                    if (idd == arrayList.get(i).getWidgetId()) {
                        if (isConnected(context)) {
                            view = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
                            view.setTextViewText(R.id.tv_widget_refresh, "Refreshing...");
                            appWidgetManager.updateAppWidget(idd, view);
//                            appWidgetManager.partiallyUpdateAppWidget(idd, view);
                            isFromSync = true;
                            new DisplayValue(context, idd, appWidgetManager, arrayList.get(i).getWidgetSource(), arrayList.get(i).getWidgetDest(), arrayList.get(i).getSrcFlag(), arrayList.get(i).getDestFlag()).execute();
                        } else {
                            Toast.makeText(context, "No internet available", Toast.LENGTH_SHORT).show();
                            isFromSync = false;
                        }
                    }
                }

            }

        } else {
            isFromSync = false;
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class DisplayValue extends AsyncTask<String, String, String> {

        String source, destination;
        StringBuffer response;
        String result;
        Context context;
        int id;
        AppWidgetManager appWidgetManager;
        Integer srcFlag, destFlag;

        DisplayValue(Context contex, int id, AppWidgetManager appWidgetManager, String source, String destination, Integer srcFlag, Integer destFlag) {
            this.source = source;
            this.destination = destination;
            this.context = contex;
            this.id = id;
            this.appWidgetManager = appWidgetManager;
            this.srcFlag = srcFlag;
            this.destFlag = destFlag;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            editor1 = sharedPreferences1.edit();
            editor1.apply();
        }

        @Override
        protected String doInBackground(String... strings) {

            Log.e(TAG, "onReceive doInBackground: " + source + " " + destination);

            String urlParameters = "&source=" + source + "&currencies=" + destination;
            String baseUrl = "https://apilayer.net/api/live?access_key=86da42ddd38aae4c13bb5b5d44f0f437";
            String url = baseUrl + urlParameters;
            URL obj = null;
            try {
                obj = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection con;
            try {
                assert obj != null;
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            /// onpostexecute
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(response.toString());
                JSONObject jsonObject1 = jsonObject.getJSONObject("quotes");
                result = "";
                result = jsonObject1.getString(source + destination);
                if (arrayList != null) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        if (arrayList.get(i).getKey().equals(source + destination)) {
                            arrayList.get(i).setResult(result);

                            editor1.clear();
                            editor1.apply();
                        }
                        Gson gson = new Gson();
                        String json = gson.toJson(arrayList);
                        editor1.putString("list", json);
                        editor1.apply();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e(TAG, "onReceive doInBackground: over" + result);
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.e(TAG, "onReceive onPostExecute: s" + s);
            Log.e(TAG, "onReceive onPostExecute: r" + result);


            view = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
            view.setTextViewText(R.id.tv_widget_refresh, "Refresh");

            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            view.setTextViewText(R.id.tv_widget_date, String.format("%s", sdf1.format(Calendar.getInstance().getTime())));

            /*    view.setImageViewResource(R.id.iv_widget_src,srcFlag);
                view.setTextViewText(R.id.tv_widget_source,  source);

                view.setImageViewResource(R.id.iv_widget_dest,  destFlag);
                view.setTextViewText(R.id.tv_widget_dest,  destination);*/

            view.setTextViewText(R.id.tv_widget_amount, result);

            appWidgetManager.updateAppWidget(id, view);
//            appWidgetManager.partiallyUpdateAppWidget(id, view);
        }
    }


    private boolean isConnected(Context context) {
        boolean connected;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
        return connected;
    }

    @Override
    public void onEnabled(final Context context) {
        super.onEnabled(context);

        isFromSync = false;

        SharedPreferences sharedPreferences = context.getSharedPreferences("widget", MODE_PRIVATE);
        view = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
        final AppWidgetManager widgetManager = AppWidgetManager.getInstance(context);

        int[] allids = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, WidgetReceiver.class));

        Gson gson = new Gson();
        String json = sharedPreferences.getString("list", "");
        Type type = new TypeToken<ArrayList<WidgetModel>>() {}.getType();
        arrayList = gson.fromJson(json, type);

        for (final int allid : allids) {
            Log.e(TAG, "onEnabled: " + allid);
            if (arrayList != null) {
                Log.e(TAG, "onEnabled: " + arrayList.size());
                for (int i = 0; i < arrayList.size(); i++) {
                    if (allid == arrayList.get(i).getWidgetId()) {
                        Log.e(TAG, "onEnabled: " + allid + " " + arrayList.get(i).getWidgetId() + " " + arrayList.get(i).getKey() + " " + arrayList.get(i).getResult());

                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        view.setTextViewText(R.id.tv_widget_date, String.format("%s", sdf1.format(Calendar.getInstance().getTime())));

                        view.setImageViewResource(R.id.iv_widget_src, arrayList.get(i).getSrcFlag());
                        view.setTextViewText(R.id.tv_widget_source, arrayList.get(i).getWidgetSource());

                        view.setImageViewResource(R.id.iv_widget_dest, arrayList.get(i).getDestFlag());
                        view.setTextViewText(R.id.tv_widget_dest, arrayList.get(i).getWidgetDest());

                        view.setTextViewText(R.id.tv_widget_amount, arrayList.get(i).getResult());

                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allid);
                        Uri data = Uri.withAppendedPath(Uri.parse("URI_SCHEME" + "://widget/id/"), String.valueOf(allid));
                        intent.setData(data);
                        PendingIntent pending = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        view.setOnClickPendingIntent(R.id.cl_widget_layout, pending);

                        Log.e(TAG, "onClick: mAppWidgetId" + allid);

                        Intent intent1 = new Intent(context, WidgetReceiver.class);
                        intent1.setAction("automaticWidgetSyncButtonClick");
                        intent1.putExtra("widgetId", allid);
                        PendingIntent pending1 = PendingIntent.getBroadcast(context, allid, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                        view.setOnClickPendingIntent(R.id.refresh_widget, pending1);

                        widgetManager.updateAppWidget(allid, view);
//                        widgetManager.partiallyUpdateAppWidget(allid, view);

                        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            ComponentName componentName = new ComponentName(context, WidgetServicePie1.class);
                            JobInfo info = new JobInfo.Builder(123, componentName)
//                    .setRequiresCharging(true)
                                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
//                    .setPersisted(true)
                                    .setOverrideDeadline((sharedPreferences.getLong("updateTime", 30)) * 60 * 1000).build();

                            JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
                            if (scheduler != null) {
                                scheduler.schedule(info);
                            }
                        } else {
                            Intent intent2 = new Intent(context, WidgetService1.class);
                            intent2.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allids);
                            context.startService(intent2);

                            final AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                            PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                            assert manager != null;
                            manager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(),
                                    (sharedPreferences.getLong("updateTime", 1)) * 60 * 1000, pendingIntent);
                            Log.e(TAG, "onUpdate: widget time " + (sharedPreferences.getLong("updateTime", 30)) * 60 * 1000);
                        }*/
                    }
                }
            }
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);

        for (int id: appWidgetIds) {

            if(arrayList!=null && arrayList.size()>0){

                for (int i = 0; i < arrayList.size(); i++) {
                    if(id == arrayList.get(i).getWidgetId()){
                        arrayList.remove(i);
                    }
                }
            }

            for (int i = 0; i < widgetModelList.size(); i++) {
                if(id == widgetModelList.get(i).getWidgetId()){
                    widgetModelList.remove(i);
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(widgetModelList);
            editor1.putString("list", json);
            editor1.apply();
        }
    }
}


