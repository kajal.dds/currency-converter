package com.example.currencyconverter.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import android.widget.RemoteViews;

import com.example.currencyconverter.R;
import com.example.currencyconverter.model.WidgetModel;
import com.example.currencyconverter.service.WidgetService1;
import com.example.currencyconverter.service.WidgetServicePie1;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.example.currencyconverter.widget.WidgetSourceActivity.widgetModelList;

/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link WidgetSourceActivity ConfigurableWidgetConfigureActivity}
 */
public class WidgetReceiver1 extends AppWidgetProvider {

    private static final String TAG = "WidgetReceiver";
    private RemoteViews view;
    private boolean isFromSync;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        Log.e(TAG, "onUpdate: in widget receiver");

        if(!isFromSync){

            SharedPreferences sharedPreferences = context.getSharedPreferences("priceValue", Context.MODE_PRIVATE);
            SharedPreferences sharedPreferences1 = context.getSharedPreferences("widget", MODE_PRIVATE);
            SharedPreferences.Editor editor1 = sharedPreferences1.edit();

            List<Integer> id = new ArrayList<>();
            for (int appWidgetId : appWidgetIds) {
                id.add(appWidgetId);
            }

            Gson gson1 = new Gson();
            String json1 = gson1.toJson(id);
            editor1.putString("widgetIdList", json1);
            editor1.apply();

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            String strSource = sharedPreferences1.getString("src", "USD");
            String strDest = sharedPreferences1.getString("dest", "INR");

            if (widgetModelList != null && widgetModelList.size() > 1) {
                Gson gson = new Gson();
                String json = sharedPreferences1.getString("list", "");
                Type type = new TypeToken<ArrayList<WidgetModel>>() {
                }.getType();
                widgetModelList = gson.fromJson(json, type);
            }

            Log.e(TAG, "onUpdate: "+strSource );
            Log.e(TAG, "onUpdate: "+strDest );

            WidgetModel widgetModel = new WidgetModel();
            widgetModel.setWidgetSource(strSource);
            widgetModel.setWidgetDest(strDest);
            widgetModel.setKey(widgetModel.getWidgetSource() + widgetModel.getWidgetDest());

            for (int appwidgetid : appWidgetIds) {
                if (widgetModelList != null) {
                    for (int j = 0; j < widgetModelList.size(); j++) {
                        if (widgetModelList.get(j).getWidgetId() != appwidgetid) {
                            widgetModel.setWidgetId(appwidgetid);
                        }
                    }
                } else {
                    widgetModel.setWidgetId(appwidgetid);
                }
            }
            widgetModelList.add(widgetModel);

            Log.e(TAG, "onUpdate: widgetModelList.size() "+widgetModelList.size() );

            Gson gson = new Gson();
            String json = gson.toJson(widgetModelList);
            editor1.putString("list", json);
            editor1.apply();

            //////////////////////////////////////////////////////////////////////////////////////////////////////

            ComponentName thisWidget = new ComponentName(context, WidgetReceiver1.class);
            int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                ComponentName componentName = new ComponentName(context, WidgetServicePie1.class);
                JobInfo info = new JobInfo.Builder(123, componentName)
//                    .setRequiresCharging(true)
                        .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
//                    .setPersisted(true)
                        .setOverrideDeadline(60 * 1000).build();

                JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
                if (scheduler != null) {
                    scheduler.schedule(info);
                }
            } else {
                Intent intent = new Intent(context, WidgetService1.class);
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);
                context.startService(intent);

                final AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                assert manager != null;
                manager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(),
                        (sharedPreferences.getLong("updateTime", 1)) * 60 * 1000, pendingIntent);
                Log.e(TAG, "onUpdate: widget time " + (sharedPreferences.getLong("updateTime", 30)) * 60 * 1000);
            }

        } else {
            isFromSync = false;
        }

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        Log.e(TAG, "onReceive: " + intent.getAction());


        String SYNC_CLICKED = "automaticWidgetSyncButtonClick";
        if (SYNC_CLICKED.equals(intent.getAction())) {
            int idd = intent.getExtras().getInt("widgetId");
            for (int i = 0; i <widgetModelList.size() ; i++) {
                Log.e(TAG, "onReceive: "+widgetModelList.get(i).getWidgetId() + widgetModelList.get(i).getKey() );
            }
            Log.e(TAG, "onReceive: "+idd );
             isFromSync = true;
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            SharedPreferences sharedPreferences = context.getSharedPreferences("priceValue", Context.MODE_PRIVATE);
            view = new RemoteViews(context.getPackageName(), R.layout.widget_layout);

            ComponentName thisWidget = new ComponentName(context, WidgetReceiver1.class);
            int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

            view.setTextViewText(R.id.tv_widget_refresh,"Refreshing...");
            appWidgetManager.updateAppWidget(thisWidget, view);

            Log.e(TAG, "onReceive: after update" );

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                    ComponentName componentName = new ComponentName(context, WidgetServicePie1.class);
                    JobInfo info = new JobInfo.Builder(123, componentName)
                            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                            .setOverrideDeadline(60 * 1000).build();

                    JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
                    if (scheduler != null) {
                        scheduler.schedule(info);
                    }
                } else {
                    Log.e(TAG, "onReceive: below oreo" );
                    Intent intent1 = new Intent(context, WidgetService1.class);
                    intent1.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);
                    context.startService(intent1);

                    final AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    assert manager != null;
                    manager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(),
                            (sharedPreferences.getLong("updateTime", 4)) * 60 * 1000, pendingIntent);
                }
        } else {
            isFromSync = false;
        }
    }

 /*   public class DisplayValue extends AsyncTask<String, String, String> {

        String source, destination;
        StringBuffer response;
        String result;

        DisplayValue(String source, String destination) {
            this.source = source;
            this.destination = destination;
        }

        @Override
        protected String doInBackground(String... strings) {

            Log.e(TAG, "doInBackground: " );

            String urlParameters = "&source=" +source + "&currencies=" + destination;
            String baseUrl = "https://apilayer.net/api/live?access_key=86da42ddd38aae4c13bb5b5d44f0f437";
            String url = baseUrl + urlParameters;
            URL obj = null;
            try {
                obj = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection con;
            try {
                assert obj != null;
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                BufferedReader in = new BufferedReader(

                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            /// onpostexecute
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(response.toString());
                JSONObject jsonObject1 = jsonObject.getJSONObject("quotes");
                result = "";

                result = jsonObject1.getString(source + destination);
                Log.e(TAG, "onPostExecute: check"+arrayList.size() );
                if (arrayList != null) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        Log.e(TAG, "onPostExecute: inn check key "+arrayList.get(i).getKey());
                        Log.e(TAG, "onPostExecute: inn check sd"+source+destination );
                        if (arrayList.get(i).getKey().equals(source + destination)) {
                            arrayList.get(i).setResult(result);
                            Log.e(TAG, "onPostExecute: inn check r"+result );

                            Gson gson = new Gson();
                            String json = gson.toJson(arrayList);
                            editor.putString("list", json);
                            editor.apply();
                        }
                        Log.e(TAG, "onPostExecute: result check"+arrayList.get(i).getKey() + "=" + arrayList.get(i).getResult() );
                    }
                }

                Gson gson1 = new Gson();
                String json1 = sharedPreferences.getString("list", "");
                Type type1 = new TypeToken<ArrayList<WidgetModel>>() {}.getType();
                arrayList = gson1.fromJson(json1, type1);

                Log.e(TAG, "onPostExecute: result" + result);
//                view.setTextViewText(R.id.tv_widget_amount, result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e(TAG, "doInBackground: over" );
            return response.toString();
        }

    }*/

}


