package com.example.currencyconverter.widget;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.currencyconverter.R;
import com.example.currencyconverter.model.CurrencyModel;
import com.example.currencyconverter.model.MainModel;
import com.example.currencyconverter.model.WidgetModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class WidgetSourceActivity extends AppCompatActivity {

    private ArrayList<MainModel> mainModelArrayList = new ArrayList<>();
    private ArrayList<CurrencyModel> tempCurrencyModelArrayList = new ArrayList<>();
    private ArrayList<CurrencyModel> currencyModelArrayList = new ArrayList<>();
    private WidgetAlphabetAdapter alphabetsAdapter;
    private ArrayList<String> tempAlphabets = new ArrayList<>();
    public static boolean isFromWidgetSrc = true;
    public static ArrayList<CurrencyModel> widgetList  = new ArrayList<>();
    public static ArrayList<WidgetModel> widgetModelList= new ArrayList<>();

    private static final String TAG = "WidgetSourceActivity";
    private int[] flags = {R.drawable.united_arab_emirates, R.drawable.afghanistan, R.drawable.albania, R.drawable.armenia, R.drawable.netherlands,
            R.drawable.angola, R.drawable.argentina, R.drawable.australia, R.drawable.aruba, R.drawable.azerbaijan, R.drawable.bosnia_herzegovina, R.drawable.barbados,
            R.drawable.bangladesh, R.drawable.bulgaria, R.drawable.bahrain, R.drawable.burundi, R.drawable.bermuda, R.drawable.brunei, R.drawable.bolivia,
            R.drawable.brazil, R.drawable.bahamas, R.drawable.bitcoin, R.drawable.bhutan, R.drawable.botswana, R.drawable.belarus, R.drawable.belize, R.drawable.canada,
            R.drawable.democratic_republi_of_congo, R.drawable.switzerland, R.drawable.chile, R.drawable.china, R.drawable.colombia,
            R.drawable.costa_rica, R.drawable.cuba, R.drawable.cape_verde, R.drawable.czech_republic, R.drawable.djibouti, R.drawable.denmark, R.drawable.dominica_republic,
            R.drawable.algeria, R.drawable.egypt, R.drawable.eritrea, R.drawable.ethiopia, R.drawable.european_union, R.drawable.fiji,
            R.drawable.falkland_islands, R.drawable.united_kingdom, R.drawable.georgia, R.drawable.guernsey, R.drawable.ghana, R.drawable.gibraltar, R.drawable.gambia,
            R.drawable.guinea, R.drawable.guatemala, R.drawable.guyana, R.drawable.hong_kong, R.drawable.honduras, R.drawable.croatia, R.drawable.haiti,
            R.drawable.hungary, R.drawable.indonesia, R.drawable.israel, R.drawable.isle_of_man, R.drawable.india, R.drawable.iraq, R.drawable.iran, R.drawable.iceland, R.drawable.jersey,
            R.drawable.jamaica, R.drawable.jordan, R.drawable.japan, R.drawable.kenya, R.drawable.kyrgyzstan, R.drawable.cambodia, R.drawable.comoros, R.drawable.north_korea,
            R.drawable.south_korea, R.drawable.kuwait, R.drawable.cayman_islands, R.drawable.kazakhstan, R.drawable.laos, R.drawable.lebanon, R.drawable.sri_lanka,
            R.drawable.liberia, R.drawable.lesotho, R.drawable.lithuania, R.drawable.latvia, R.drawable.libya, R.drawable.morocco, R.drawable.moldova, R.drawable.madagascar,
            R.drawable.republic_of_macedonia, R.drawable.myanmar, R.drawable.mongolia, R.drawable.macao, R.drawable.mauritius, R.drawable.mauritius, R.drawable.maldives, R.drawable.malawi,
            R.drawable.mexico, R.drawable.malaysia, R.drawable.mozambique, R.drawable.namibia, R.drawable.nigeria, R.drawable.nicaragua, R.drawable.norway,
            R.drawable.nepal, R.drawable.new_zealand, R.drawable.oman, R.drawable.panama, R.drawable.peru, R.drawable.papua_new_guinea, R.drawable.philippines, R.drawable.pakistan, R.drawable.republic_of_poland,
            R.drawable.paraguay, R.drawable.qatar, R.drawable.romania, R.drawable.serbia, R.drawable.russia, R.drawable.rwanda, R.drawable.saudi_arabia, R.drawable.solomon_islands,
            R.drawable.seychelles, R.drawable.sudan, R.drawable.sweden, R.drawable.singapore, R.drawable.british_virgin_islands, R.drawable.sierra_leone, R.drawable.somalia, R.drawable.suriname,
            R.drawable.sao_tome_and_principe, R.drawable.salvador, R.drawable.syria, R.drawable.swaziland, R.drawable.thailand, R.drawable.tajikistan, R.drawable.turkmenistan,
            R.drawable.tunisia, R.drawable.tonga, R.drawable.turkey, R.drawable.trinidad_and_tobago, R.drawable.taiwan, R.drawable.tanzania, R.drawable.ukraine,
            R.drawable.uganda, R.drawable.united_states_of_america, R.drawable.uruguay, R.drawable.uzbekistn, R.drawable.venezuela, R.drawable.vietnam,
            R.drawable.vanuatu, R.drawable.samoa, R.drawable.silver, R.drawable.silver, R.drawable.gold, R.drawable.saint_kitts_and_nevis, R.drawable.silver, R.drawable.silver,
            R.drawable.silver, R.drawable.yemen, R.drawable.south_africa, R.drawable.zambia, R.drawable.zimbabwe};// R.drawable.chile

    private String[] symbols = {"د.إ", " ؋", "Lek", "֏", "ƒ", "Kz", "$", "$", "ƒ", "\u20BC", "KM", "$", "৳", "лв", ".د.ب", "FBu", "$", "B$", "Bs", "R$", "$", "\u20BF", "Nu.", "P", "Br", "BZ$",     // a and b
            "$", "FC", "CHF", "UF", "¥", "$", "₡", "₱", "$", "Kč", "Fdj", "Kr.", "$", "دج", "£", "Nfk", "Br", "€", "$", "£", "£", "GEL", "£", "GH¢", "£", "D", "FG", "Q", "$",         // c to g
            "$", "L", "kn", "G", "Ft", "Rp", "₪", "£", "\u20B9", "ع.د", "﷼", "kr", "£", "$", "د.ا", "¥", "K", "Лв", "៛", "CF", "₩", "₩", "KD", "$", "₸", "₭N", "ل.ل", "Rs", "L$", "L", "Lt", "Ls", "LD",        //h to l
            ". د. م.", "L", "Ar", "Ден", "K", "₮", "MOP$", "UM", "₨", "MVR", "MK", "$", "RM", "MT", "N$", "₦", "C$", "kr", "Rs", "$", "ر.ع.", "B/.", "S/", "K", "₱", "₨", "zł", "₲", "QR",              //m to q
            "lei", "din", "₽", "RF", "ر.س", "Si$", "SRe", "ج.س", "kr", "$", "£", "Le", "S", "$", "Db", "C", "£S", "L", "฿", "ЅM", "T", "DT", "T$", "\u20BA", "TT$", "NT$", "TSh",                                      //s to t
            "₴", "USh", "$", "$U", "so'm", "Bs", "₫", "VT", "WS$", "FCFA", "XAG", "XAU", "$", "XDR", "CFA", "₣", "﷼", "R", "ZK", "Z$"};

    private String[] alphabets = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_all_currencies);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Log.e(TAG, "onCreate: length" + flags.length);
        Log.e(TAG, "onCreate: length sym" + symbols.length);
//        widgetList  =  new ArrayList<>();

        Toast.makeText(this, "Source", Toast.LENGTH_SHORT).show();

        SharedPreferences sharedPreferencesw = getSharedPreferences("widget", Context.MODE_PRIVATE);

   /*     if(widgetModelList != null && widgetModelList.size() != 0){
            Log.e(TAG, "onCreate: ww not null" );
            Gson gson = new Gson();
            String json = sharedPreferencesw.getString("list", "");
            Type type = new TypeToken<ArrayList<WidgetModel>>() {}.getType();
            widgetModelList = gson.fromJson(json, type);

            for (int i = 0; i < widgetModelList.size(); i++) {
                Log.e(TAG, "onCreate: www " + widgetModelList.get(i).getKey() + "==" + widgetModelList.get(i).getResult());
            }

        } else {
            Log.e(TAG, "onCreate: ww null" );

        }*/


       /* Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG, "onCreate: widgetttt"+ MainActivity.allPrices.size());
            }
        },5000);*/


        final Activity activity = WidgetSourceActivity.this;
        final RecyclerView rvAlphabets = findViewById(R.id.rv_alphabets);
        final EditText etSearch = findViewById(R.id.et_search);
        ImageView ivBack = findViewById(R.id.iv_back);

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        rvAlphabets.setLayoutManager(layoutManager);

        getJsonData();
        MainModel mainModel;
        for (String alphabet : alphabets) {
            mainModel = new MainModel();
            mainModel.setLetter(alphabet);
            currencyModelArrayList = new ArrayList<>();
            for (int j = 0; j < tempCurrencyModelArrayList.size(); j++) {
                if (tempCurrencyModelArrayList.get(j).getCurrencyCode().substring(0, 1).equals(alphabet)) {
                    Log.e(TAG, "onCreate: if--->");
                    CurrencyModel currencyModel = new CurrencyModel();
                    currencyModel.setCurrencyCode(tempCurrencyModelArrayList.get(j).getCurrencyCode());
                    currencyModel.setCurrencyName(tempCurrencyModelArrayList.get(j).getCurrencyName());
                    currencyModel.setFlag(tempCurrencyModelArrayList.get(j).getFlag());
                    currencyModel.setCurrencySymbol(tempCurrencyModelArrayList.get(j).getCurrencySymbol());
                    currencyModelArrayList.add(currencyModel);
                    Log.e(TAG, "onCreate: " + currencyModelArrayList.size());
                }
            }
            mainModel.setCurrencyModelArrayList(currencyModelArrayList);
            mainModelArrayList.add(mainModel);
        }

        alphabetsAdapter = new WidgetAlphabetAdapter(activity, mainModelArrayList);
        rvAlphabets.setAdapter(alphabetsAdapter);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().equals("")) {
                    Log.e(TAG, "onTextChanged: blank");
                    tempCurrencyModelArrayList = new ArrayList<>();
                    currencyModelArrayList = new ArrayList<>();
                    mainModelArrayList = new ArrayList<>();
                    getJsonData();
                    MainModel mainModel;
                    for (String alphabet : alphabets) {
                        mainModel = new MainModel();
                        mainModel.setLetter(alphabet);
                        currencyModelArrayList = new ArrayList<>();
                        for (int j = 0; j < tempCurrencyModelArrayList.size(); j++) {
                            if (tempCurrencyModelArrayList.get(j).getCurrencyCode().substring(0, 1).equals(alphabet)) {
                                CurrencyModel currencyModel = new CurrencyModel();
                                currencyModel.setCurrencyCode(tempCurrencyModelArrayList.get(j).getCurrencyCode());
                                currencyModel.setCurrencyName(tempCurrencyModelArrayList.get(j).getCurrencyName());
                                currencyModel.setFlag(tempCurrencyModelArrayList.get(j).getFlag());
                                currencyModel.setCurrencySymbol(tempCurrencyModelArrayList.get(j).getCurrencySymbol());
                                currencyModelArrayList.add(currencyModel);
                            }
                        }
                        mainModel.setCurrencyModelArrayList(currencyModelArrayList);
                        mainModelArrayList.add(mainModel);
                    }
                } else {

                    Log.e(TAG, "onTextChanged: else");
                    mainModelArrayList = new ArrayList<>();
                    tempAlphabets = new ArrayList<>();
                    currencyModelArrayList = new ArrayList<>();

                    // TODO: 25-11-2019 Get alphabets input string wise
                    for (int j = 0; j < tempCurrencyModelArrayList.size(); j++) {
                        if (tempCurrencyModelArrayList.get(j).getCurrencyCode().toLowerCase().contains(s.toString().toLowerCase()) ||
                                tempCurrencyModelArrayList.get(j).getCurrencyName().toLowerCase().contains(s.toString().toLowerCase())) {
                            Log.e(TAG, "onCreate: if--->");
                            String first1 = tempCurrencyModelArrayList.get(j).getCurrencyCode().substring(0, 1);
                            Log.e(TAG, "onTextChanged: firstyy" + first1);
                            if (!tempAlphabets.contains(first1)) {
                                tempAlphabets.add(first1);
                            }
                            Log.e(TAG, "onTextChanged: tempyy" + tempAlphabets.toString() + " " + tempAlphabets.size());
                        }
                    }

                    // TODO: 25-11-2019 Get data alphabets wise
                    for (int i = 0; i < tempAlphabets.size(); i++) {
                        MainModel finalModel = new MainModel();
                        finalModel.setLetter(tempAlphabets.get(i));
                        currencyModelArrayList = new ArrayList<>();
                        Log.e(TAG, "onTextChanged: temp size" + tempCurrencyModelArrayList.size());

                        for (int k = 0; k < tempCurrencyModelArrayList.size(); k++) {
                            if (tempCurrencyModelArrayList.get(k).getCurrencyCode().toLowerCase().contains(s.toString().toLowerCase()) ||
                                    tempCurrencyModelArrayList.get(k).getCurrencyName().toLowerCase().contains(s.toString().toLowerCase())) {
                                if (tempCurrencyModelArrayList.get(k).getCurrencyCode().substring(0, 1).equals(tempAlphabets.get(i))) {
                                    CurrencyModel currencyModel = new CurrencyModel();
                                    currencyModel.setCurrencyCode(tempCurrencyModelArrayList.get(k).getCurrencyCode());
                                    currencyModel.setCurrencyName(tempCurrencyModelArrayList.get(k).getCurrencyName());
                                    currencyModel.setFlag(tempCurrencyModelArrayList.get(k).getFlag());
                                    currencyModel.setCurrencySymbol(tempCurrencyModelArrayList.get(k).getCurrencySymbol());
                                    currencyModelArrayList.add(currencyModel);
                                    Log.e(TAG, "onTextChanged: currencymodel size " + currencyModelArrayList.size());
                                    Log.e(TAG, "onTextChanged:code" + currencyModel.getCurrencyCode());
                                }
                            }
                        }

                        finalModel.setCurrencyModelArrayList(currencyModelArrayList);
                        mainModelArrayList.add(finalModel);
                    }

                    /*        CurrencyModel currencyModel = new CurrencyModel();
                            currencyModel.setCurrencyCode(tempCurrencyModelArrayList.get(j).getCurrencyCode());
                            currencyModel.setCurrencyName(tempCurrencyModelArrayList.get(j).getCurrencyName());
                            currencyModel.setFlag(tempCurrencyModelArrayList.get(j).getFlag());
                            currencyModelArrayList.add(currencyModel);*/

                        /*    for (int i = 0; i < currencyModelArrayList.size(); i++) {
                                String first = currencyModelArrayList.get(i).getCurrencyCode().substring(0, 1);
                                    MainModel mainModel = new MainModel();
                                    mainModel.setLetter(first);
                                    mainModel.setCurrencyModelArrayList(currencyModelArrayList);
                                    Log.e(TAG, "onTextChanged: letter = 2" + mainModel.getLetter());
                                    mainModelArrayList.add(mainModel);
//                                }
                            }
                            currencyModelArrayList = new ArrayList<>();*/
//                            Log.e(TAG, "onTextChanged: code = " + currencyModel.getCurrencyCode() + " name= " + currencyModel.getCurrencyName());


                }
                Log.e(TAG, "onTextChanged: data set complete" + mainModelArrayList.size());
                alphabetsAdapter = new WidgetAlphabetAdapter(activity, mainModelArrayList);
                rvAlphabets.setAdapter(alphabetsAdapter);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    private String loadJSON() {
        String json;
        try {
            InputStream is = getAssets().open("currency");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
            Log.e(TAG, "loadJSON: response" + json);
        } catch (IOException ex) {
            ex.printStackTrace();
            Log.e(TAG, "loadJSON: catch" + ex.getMessage());
            return null;
        }
        return json;
    }

    private void getJsonData() {

        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(loadJSON());
            Log.e(TAG, "getJsonData: " + jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String currencyCode = jsonObject.getString("code");
                String currencyName = jsonObject.getString("name");
                CurrencyModel currencyModel = new CurrencyModel();
                currencyModel.setCurrencyCode(currencyCode);
                currencyModel.setCurrencyName(currencyName);
                currencyModel.setFlag(flags[i]);
                currencyModel.setCurrencySymbol(symbols[i]);
                tempCurrencyModelArrayList.add(currencyModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
