package com.example.currencyconverter.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.currencyconverter.R;
import com.example.currencyconverter.activity.MainActivity;
import com.example.currencyconverter.model.CurrencyModel;
import com.example.currencyconverter.model.WidgetModel;
import com.example.currencyconverter.utils.ObjectSerializer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.example.currencyconverter.widget.WidgetSourceActivity.widgetModelList;


public class WidgetAdapter extends RecyclerView.Adapter<WidgetAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<CurrencyModel> currencyModelArrayList;
    private static final String TAG = "AllCurrenciesAdapter";
    private RemoteViews view;
    private AppWidgetManager widgetManager;
    private int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    private String baseUrl = "https://apilayer.net/api/live?access_key=86da42ddd38aae4c13bb5b5d44f0f437";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Double result;
//    private HashMap<String,String> updateAmount = new HashMap<>();


    public WidgetAdapter(Context mContext, ArrayList<CurrencyModel> currencyModelArrayList) {
        this.mContext = mContext;
        this.currencyModelArrayList = currencyModelArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_all_currency, parent, false);

        view = new RemoteViews(mContext.getPackageName(), R.layout.widget_layout);
        widgetManager = AppWidgetManager.getInstance(mContext);
        Intent intent = ((Activity) mContext).getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
            Log.e(TAG, "onCreateViewHolder: mAppWidgetId-->"+mAppWidgetId );
        }
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            ((Activity) mContext).finish();
        }

        ArrayList<WidgetModel> ss = new ArrayList<>();
        ss.clear();

        sharedPreferences = mContext.getSharedPreferences("widget", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("list", "");
        Type type = new TypeToken<ArrayList<WidgetModel>>() {}.getType();
         ss = gson.fromJson(json, type);
        if(ss!=null && ss.size()>0){
            widgetModelList.clear();
            Log.e(TAG, "onCreateViewHolder: ss"+ss.size() );
            for (int i = 0; i <ss.size() ; i++) {
                widgetModelList.addAll(ss);
            }
        }

        return new MyViewHolder(v);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        Glide.with(mContext)
                .load(currencyModelArrayList.get(position).getFlag())
                .into(holder.ivFlag);
        holder.tvCurrencyCode.setText(currencyModelArrayList.get(position).getCurrencyCode());
        holder.tvCurrencyName.setText(currencyModelArrayList.get(position).getCurrencyName());

        holder.ivSelect.setVisibility(View.GONE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                sharedPreferences = mContext.getSharedPreferences("widget", MODE_PRIVATE);
                editor = sharedPreferences.edit();

                if (holder.ivSelect.isShown()) {
                    Toast.makeText(mContext, "Please select another currency", Toast.LENGTH_SHORT).show();
                } else {
                    holder.ivSelect.setVisibility(View.VISIBLE);
                    WidgetSourceActivity.widgetList.add(currencyModelArrayList.get(position));

                    Log.e(TAG, "onClick: size" + WidgetSourceActivity.widgetList.size());

                    if (WidgetSourceActivity.widgetList.size() == 2) {

                        String sourceValue = "1";
                        String destValue = "72";
                        SharedPreferences priceSharedPreferences = mContext.getSharedPreferences("allPriceList", MODE_PRIVATE);
                        try {
                            HashMap<String, String> tempPriceList = (HashMap<String, String>) ObjectSerializer.deserialize(priceSharedPreferences.getString("myPriceList", ObjectSerializer.serialize(new HashMap<String, String>())));
                            assert tempPriceList != null;
                            Log.e(TAG, "onCreate: widgetttt" + tempPriceList.size());

                            for (Map.Entry<String, String> entry : tempPriceList.entrySet()) {
//                                Log.e(TAG, "onClick: result"+entry.getKey()+ entry.getValue() );
                                if (entry.getKey().equals(WidgetSourceActivity.widgetList.get(0).getCurrencyCode())) {
                                    sourceValue = entry.getValue();
                                }
                                if (entry.getKey().equals(WidgetSourceActivity.widgetList.get(1).getCurrencyCode())) {
                                    destValue = entry.getValue();
                                }
                            }
                            Log.e(TAG, "onClick: result s =" + sourceValue + " d = " + destValue);
                            result = Double.parseDouble(destValue) / Double.parseDouble(sourceValue);
                            view.setTextViewText(R.id.tv_widget_amount, String.format(Locale.getDefault(), "%.5f", result));

                            Intent intent1 = new Intent(mContext, WidgetReceiver.class);
                            intent1.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                            int[] ids = AppWidgetManager.getInstance(mContext).getAppWidgetIds(new ComponentName(mContext, WidgetReceiver.class));
                            intent1.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
                            mContext.sendBroadcast(intent1);

                        } catch (IOException | ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                        Log.e(TAG, "onClick: result" + result);

//                        Toast.makeText(mContext, "We are in update widget", Toast.LENGTH_SHORT).show();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss",Locale.ENGLISH);
                        view.setTextViewText(R.id.tv_widget_date, String.format("%s", sdf1.format(Calendar.getInstance().getTime())));

                        view.setImageViewResource(R.id.iv_widget_src, WidgetSourceActivity.widgetList.get(0).getFlag());
                        view.setTextViewText(R.id.tv_widget_source, WidgetSourceActivity.widgetList.get(0).getCurrencyCode());

                        view.setImageViewResource(R.id.iv_widget_dest, WidgetSourceActivity.widgetList.get(1).getFlag());
                        view.setTextViewText(R.id.tv_widget_dest, WidgetSourceActivity.widgetList.get(1).getCurrencyCode());

                        editor.putString("src", WidgetSourceActivity.widgetList.get(0).getCurrencyCode());
                        editor.putString("dest", WidgetSourceActivity.widgetList.get(1).getCurrencyCode());

                        editor.putInt("srcFlag",WidgetSourceActivity.widgetList.get(0).getFlag());
                        editor.putInt("destFlag",WidgetSourceActivity.widgetList.get(1).getFlag());

                        editor.apply();

                        WidgetModel widgetModel = new WidgetModel();
                        widgetModel.setWidgetSource(WidgetSourceActivity.widgetList.get(0).getCurrencyCode());
                        widgetModel.setWidgetDest(WidgetSourceActivity.widgetList.get(1).getCurrencyCode());
                        widgetModel.setSrcFlag(WidgetSourceActivity.widgetList.get(0).getFlag());
                        widgetModel.setDestFlag(WidgetSourceActivity.widgetList.get(1).getFlag());
                        widgetModel.setKey(WidgetSourceActivity.widgetList.get(0).getCurrencyCode() + WidgetSourceActivity.widgetList.get(1).getCurrencyCode());
                        widgetModel.setWidgetId(mAppWidgetId);

                        widgetModelList.add(widgetModel);

                        Gson gson = new Gson();
                        String json = gson.toJson(widgetModelList);
                        editor.putString("list", json);
                        editor.apply();

                        Log.e(TAG, "onClick: af"+widgetModelList.size());

                        Intent intent = new Intent(mContext, MainActivity.class);
                        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, mAppWidgetId);
                        Uri data = Uri.withAppendedPath(Uri.parse("URI_SCHEME" + "://widget/id/"), String.valueOf(mAppWidgetId));
                        intent.setData(data);
                        PendingIntent pending = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        view.setOnClickPendingIntent(R.id.cl_widget_layout, pending);

                        Log.e(TAG, "onClick: mAppWidgetId"+mAppWidgetId );

                        Intent intent1 = new Intent(mContext, WidgetReceiver.class);
                        intent1.setAction("automaticWidgetSyncButtonClick");
                        intent1.putExtra("widgetId",mAppWidgetId);
                        PendingIntent pending1 = PendingIntent.getBroadcast(mContext, mAppWidgetId, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                        view.setOnClickPendingIntent(R.id.refresh_widget, pending1);

                        widgetManager.updateAppWidget(mAppWidgetId, view);
//                        widgetManager.partiallyUpdateAppWidget(mAppWidgetId, view);

                        Intent resultValue = new Intent();
                        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
                        ((Activity) mContext).setResult(RESULT_OK, resultValue);
                        ((Activity) mContext).finish();
                        WidgetSourceActivity.widgetList.clear();
                    }
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return currencyModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivFlag, ivSelect;
        private TextView tvCurrencyCode, tvCurrencyName;
        View itemView;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            ivFlag = itemView.findViewById(R.id.item_iv_flag);
            ivSelect = itemView.findViewById(R.id.iv_select);
            tvCurrencyCode = itemView.findViewById(R.id.item_tv_currency_code);
            tvCurrencyName = itemView.findViewById(R.id.item_tv_currency_name);
        }
    }

    @SuppressLint("StaticFieldLeak")
    class DisplayValueInWidget extends AsyncTask<String, String, String> {
        StringBuffer response;
        String source, destination;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            source = WidgetSourceActivity.widgetList.get(0).getCurrencyCode();
            destination = WidgetSourceActivity.widgetList.get(1).getCurrencyCode();
        }

        @Override
        protected String doInBackground(String... strings) {
            String urlParameters = "&source=" + source + "&currencies=" + destination;
            String url = baseUrl + urlParameters;
            URL obj = null;
            try {
                obj = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpURLConnection con;
            try {
                assert obj != null;
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("quotes");
//                result = jsonObject1.getString(source + destination);
//                 updateAmount.put(source+destination,result);
                Log.e(TAG, "onPostExecute: result" + source + destination + "=" + result);

                Intent intent = new Intent(mContext, WidgetReceiver.class);
                intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                int[] ids = AppWidgetManager.getInstance(mContext).getAppWidgetIds(new ComponentName(mContext, WidgetReceiver.class));
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
                mContext.sendBroadcast(intent);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
