package com.example.currencyconverter.fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.example.currencyconverter.graph.CustomMarkerView;
import com.example.currencyconverter.R;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChartFragment extends Fragment implements OnChartGestureListener, OnChartValueSelectedListener, View.OnClickListener {

    private static final String TAG = "ChartFragment";
    private ProgressDialog progressDialog;
    private LineChart mChart;
    private ArrayList<String> xAxisLabels = new ArrayList<>();
    private ArrayList<Entry> yAxisLabels = new ArrayList<>();
    private String startDate, endDate, strSource, strDestination;
    private LinearLayout llNoInternet;

    public ChartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_chart, container, false);
        Log.e(TAG, "onCreateView: str" + strSource + strDestination);

        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        startDate = getOneYearAgoDate(sdf.format(calendar.getTime()));
        endDate = sdf.format(calendar.getTime());
        Log.e(TAG, "onCreateView: getOneYearAgoDate" + getOneYearAgoDate(sdf.format(calendar.getTime())));

        llNoInternet = v.findViewById(R.id.ll_no_internet);
        llNoInternet.setOnClickListener(this);
        llNoInternet.setVisibility(View.GONE);

        mChart = v.findViewById(R.id.linechart);
        mChart.setOnChartGestureListener(this);
        mChart.setOnChartValueSelectedListener(this);
        mChart.setDrawGridBackground(false);

        return v;
    }


    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        if (lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            mChart.highlightValues(null);
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: ->" );

        SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences("convert", MODE_PRIVATE);

        /*strSource = getArguments().getString("source");
        strDestination = getArguments().getString("destination");*/

        strSource = sharedPreferences.getString("graphSource","USD");
        strDestination = sharedPreferences.getString("graphDest","INR");

        xAxisLabels = new ArrayList<>();
        yAxisLabels = new ArrayList<>();

        if(isConnected(getActivity())){
            mChart.setVisibility(View.VISIBLE);
            llNoInternet.setVisibility(View.GONE);
            new DisplayCurrencyValue().execute();
        } else {
            mChart.setVisibility(View.GONE);
            llNoInternet.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), "Please connect to internet", Toast.LENGTH_LONG).show();
        }
        Log.e(TAG, "onResume: ==>"+ strSource + " " + strDestination + " " + startDate + " " + endDate  );

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.ll_no_internet){
            if(isConnected((getActivity()))){
                mChart.setVisibility(View.VISIBLE);
                new DisplayCurrencyValue().execute();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }
        }
    }


     private boolean isConnected(Context context) {
        /*ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
         assert cm != null;
         NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();*/

        boolean connected;
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
         //we are connected to a network
         assert connectivityManager != null;
         connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                 connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
         return connected;
    }


    @SuppressLint("StaticFieldLeak")
     class DisplayCurrencyValue extends AsyncTask<String, String, String> {

        StringBuffer response;
        ArrayList<String> yvalues = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Processing..");
            progressDialog.setCancelable(false);
            progressDialog.show();

            llNoInternet.setVisibility(View.GONE);
        }

        @Override
        protected String doInBackground(String... strings) {

            String urlParameters = "&source=" + strSource + "&currencies=" + strDestination + "&start_date=" + startDate + "&end_date=" + endDate;
            String baseUrl = "https://apilayer.net/api/timeframe?access_key=86da42ddd38aae4c13bb5b5d44f0f437";

            Log.e(TAG, "doInBackground: str" + strSource + " " + strDestination);

            String url = baseUrl + urlParameters;
            URL obj = null;
            try {
                obj = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection con;
            try {
                assert obj != null;
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                System.out.println(response.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.e(TAG, "onPostExecute: response" + s);

            progressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("quotes");

                Iterator keys = jsonObject1.keys();

                while (keys.hasNext()) {
                    String currentDynamicKey = (String) keys.next();
                    Log.e(TAG, "onPostExecute: yap" + currentDynamicKey);

                    xAxisLabels.add(currentDynamicKey);
                    Log.e(TAG, "onPostExecute: "+xAxisLabels.size() );

                    JSONObject jsonObject2 = jsonObject1.getJSONObject(currentDynamicKey);
                    String value = jsonObject2.getString(strSource+strDestination);
                    yAxisLabels.add(new Entry(Float.parseFloat(value),xAxisLabels.size()));
                    yvalues.add(value);
                    Log.e(TAG, "onPostExecute: yay" + jsonObject2.toString() + value);
                    Log.e(TAG, "onPostExecute: yaxis"+yAxisLabels );
                }

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setData();
                        CustomMarkerView mv = new CustomMarkerView(getActivity(), R.layout.custom_marker_view, xAxisLabels);
                        mv.setChartView(mChart);
                        mChart.setMarkerView(mv);

                        Legend l = mChart.getLegend();
                        l.setForm(Legend.LegendForm.LINE);
                        mChart.setTouchEnabled(true);
                        mChart.setDragEnabled(true);
                        mChart.setScaleEnabled(true);

                        YAxis leftAxis = mChart.getAxisLeft();
          /*              leftAxis.setLabelCount(5,true);
                        leftAxis.setAxisMinValue(Float.parseFloat(yvalues.get(0)));
                        leftAxis.setAxisMaxValue(Float.parseFloat(yvalues.get(yvalues.size()-1)));*/
                        leftAxis.removeAllLimitLines();
                        leftAxis.enableGridDashedLine(10f, 10f, 0f);
                        leftAxis.setDrawZeroLine(false);
                        leftAxis.setDrawLimitLinesBehindData(true);

                        mChart.getAxisRight().setEnabled(true);
                        mChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
                        mChart.invalidate();
                    }
                },100);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private String getOneYearAgoDate(String dateString) {

        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        assert myDate != null;
        calendar.setTime(myDate);
        calendar.add(Calendar.DAY_OF_YEAR, -365);
        Date newDate = calendar.getTime();

        return dateFormat.format(newDate);
    }

    private void setData() {
        LineDataSet set1;

        // create a dataset and give it a type
        set1 = new LineDataSet(yAxisLabels, "");

        set1.setFillColor(Color.parseColor("#7DB049"));
       set1.setFillAlpha(50);

        // set the line to be drawn like this "- - - - - -"
//            set1.enableDashedLine(10f, 5f, 0f);
//            set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(Color.BLACK);
        set1.setCircleColor(Color.BLACK);
        set1.setLineWidth(1f);
        set1.setCircleRadius(0f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(0f);
        set1.setDrawFilled(true);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(xAxisLabels, dataSets);

        // set data
        mChart.setData(data);

    }

}
