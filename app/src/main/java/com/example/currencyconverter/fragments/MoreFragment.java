package com.example.currencyconverter.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.currencyconverter.BuildConfig;
import com.example.currencyconverter.R;
import com.example.currencyconverter.activity.CurrencyProfileActivity;
import com.example.currencyconverter.activity.MainActivity;

import java.util.Locale;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoreFragment extends Fragment implements View.OnClickListener {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    public static boolean isFromLanguage = false;

    public MoreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_more, container, false);

        sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences("Language", Context.MODE_PRIVATE);
        editor =  sharedPreferences.edit();
        editor.apply();

        loadLocale();

        LinearLayout llRateAlert = v.findViewById(R.id.ll_rate_alerts);
        LinearLayout llCurrencyProfile = v.findViewById(R.id.ll_currency_profile);
        LinearLayout llLanguage = v.findViewById(R.id.ll_language);
        LinearLayout llShareApp = v.findViewById(R.id.ll_share_app);
        LinearLayout llWebsite = v.findViewById(R.id.ll_website);
        LinearLayout llContactUs = v.findViewById(R.id.ll_contact);

        llRateAlert.setOnClickListener(this);
        llCurrencyProfile.setOnClickListener(this);
        llLanguage.setOnClickListener(this);
        llShareApp.setOnClickListener(this);
        llWebsite.setOnClickListener(this);
        llContactUs.setOnClickListener(this);

        return v;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ll_rate_alerts:
                MainActivity.activity.tvDateTime.setVisibility(View.GONE);
                MainActivity.activity.llDisplayCurrency.setVisibility(View.GONE);
                MainActivity.activity.rvCurrency.setVisibility(View.GONE);
                MainActivity.activity.llFragmentContainer.setVisibility(View.VISIBLE);
                MainActivity.activity.ivManualRefresh.setVisibility(View.GONE);
                MainActivity.activity.llGraphText.setVisibility(View.VISIBLE);
                MainActivity.activity.fabAdd.setVisibility(View.GONE);
                FragmentManager manager2 = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
                FragmentTransaction transaction2 = manager2.beginTransaction();
                transaction2.replace(R.id.ll_fragment_container, new CreateAlertFragment()).commit();
                break;

            case R.id.ll_currency_profile:
                Intent intent = new Intent(getActivity(), CurrencyProfileActivity.class);
                startActivity(intent);
                break;

            case R.id.ll_language:
                showLanguageDialog();
                break;

            case R.id.ll_share_app:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;

            case R.id.ll_website:
                String url1;
                if(sharedPreferences.getString("mylanguage","en").equals("ar")){
                     url1 = "https://www.currencyc.com/ar/";
                } else {
                     url1 = "https://www.currencyc.com";
                }
                Intent i1 = new Intent(Intent.ACTION_VIEW);
                i1.setData(Uri.parse(url1));
                startActivity(i1);
                break;

            case R.id.ll_contact:
                String url = "https://www.currencyc.com/contact.html";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
        }
    }

    private void showLanguageDialog(){

        final String[] listItems = {"English", "Arabic"};
        AlertDialog.Builder  mBuilder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        mBuilder.setTitle("Choose Language");
        mBuilder.setSingleChoiceItems(listItems, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {

                if( i == 0){
                    setLocale("en");
                    Intent i1 = Objects.requireNonNull(getActivity()).getBaseContext().getPackageManager().
                            getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName());
                    assert i1 != null;
                    i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i1);
                    getActivity().overridePendingTransition(0, 0);
//                    Objects.requireNonNull(getActivity()).recreate();
                }

                if(i==1){
                    setLocale("ar");
                    Intent i1 = Objects.requireNonNull(getActivity()).getBaseContext().getPackageManager().
                            getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName());
                    assert i1 != null;
                    i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i1);
                    getActivity().overridePendingTransition(0, 0);
//                    Objects.requireNonNull(getActivity()).recreate();
                }
                isFromLanguage = true;
                dialog.dismiss();
            }
        });

        AlertDialog mDialog = mBuilder.create();
        mDialog.show();

    }


    private void setLocale(String language){
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration config = res.getConfiguration();
        config.setLocale(new Locale(language));
//        Objects.requireNonNull(getActivity()).getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
        res.updateConfiguration(config,dm);
        editor.putString("mylanguage",language);
        editor.apply();
    }

/*    private void setLocale(String language){
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        Objects.requireNonNull(getActivity()).getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
        editor.putString("mylanguage",language);
        editor.apply();
    }*/

    private void loadLocale(){
        String language = sharedPreferences.getString("mylanguage","en");
        setLocale(language);
    }
}
