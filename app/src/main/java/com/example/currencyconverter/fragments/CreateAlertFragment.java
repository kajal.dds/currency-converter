package com.example.currencyconverter.fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.currencyconverter.R;
import com.example.currencyconverter.adapter.CreateAlertAdapter;
import com.example.currencyconverter.model.CreateAlertModel;
import com.example.currencyconverter.receiver.AlarmReceiver;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateAlertFragment extends Fragment implements View.OnClickListener {


    private EditText etAlertAmount;
    private ArrayList<CreateAlertModel> list = new ArrayList<>();
    private SharedPreferences sharedPreferences,  sharedPreferences1;
    private SharedPreferences.Editor mEditor;
    private RecyclerView rvAlert;
    private static final String TAG = "CreateAlertFragment";
    private String baseUrl = "https://apilayer.net/api/live?access_key=86da42ddd38aae4c13bb5b5d44f0f437";
    String from, to, result;

    public CreateAlertFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_create_alert, container, false);

        sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences("clickCount", MODE_PRIVATE);
        mEditor = sharedPreferences.edit();
        mEditor.apply();

         sharedPreferences1 = getActivity().getSharedPreferences("priceValue", Context.MODE_PRIVATE);


        etAlertAmount = v.findViewById(R.id.et_alert_amount);
        TextView tvCreateAlert = v.findViewById(R.id.tv_create_alert);

        rvAlert = v.findViewById(R.id.rv_alerts);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvAlert.setLayoutManager(layoutManager);

        etAlertAmount.setText(sharedPreferences.getString("alertamount", "70.0"));

        Gson gson = new Gson();
        String json = sharedPreferences.getString("list", "");
        Type type = new TypeToken<ArrayList<CreateAlertModel>>() {}.getType();
        ArrayList arrayList = gson.fromJson(json, type);
        Log.e(TAG, "onCreateView: arraylist" + arrayList);
        if (arrayList != null) {
            list = gson.fromJson(json, type);
            CreateAlertAdapter adapter = new CreateAlertAdapter(getActivity(), list);
            rvAlert.setAdapter(adapter);

           /* Intent intent = new Intent(getActivity(), AlarmReceiver.class);
            getActivity().sendBroadcast(intent);*/
        }




    /*    Gson gson = new Gson();
        list = gson.fromJson(sharedPreferences.getString("list",""), new TypeToken<ArrayList<CreateAlertModel>>() {}.getType());*/
/*        try {
                list = (ArrayList<CreateAlertModel>) ObjectSerializer.deserialize(sharedPreferences.getString("list", ObjectSerializer.serialize(new ArrayList<CreateAlertModel>())));
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "onCreateView: arraylist"+list );
        CreateAlertAdapter adapter = new CreateAlertAdapter(getActivity(), list);
        rvAlert.setAdapter(adapter);*/
        tvCreateAlert.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.tv_create_alert) {

            mEditor.putString("alertamount", etAlertAmount.getText().toString());
            mEditor.apply();

            from = sharedPreferences.getString("graphSource", "USD");
            to = sharedPreferences.getString("graphDest", "INR");

            new DisplayCurrencyValue().execute();

            if(list!=null && list.size()<5){
            Intent intent = new Intent(getActivity(),AlarmReceiver.class);
            Objects.requireNonNull(getActivity()).sendBroadcast(intent);
            }

            /*if (list != null) {
                AlarmManager alarmManager = (AlarmManager) Objects.requireNonNull(getActivity()).getSystemService(ALARM_SERVICE);
                Intent alarmIntent = new Intent(getActivity(), AlarmReceiver.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                alarmIntent.setData((Uri.parse("custom://" + System.currentTimeMillis())));
                assert alarmManager != null;
                alarmManager.cancel(pendingIntent);

                Calendar alarmStartTime = Calendar.getInstance();
                Calendar now = Calendar.getInstance();
              *//*  alarmStartTime.set(Calendar.HOUR_OF_DAY,  now.get(Calendar.HOUR));
                alarmStartTime.set(Calendar.MINUTE,  now.get(Calendar.MINUTE));
                alarmStartTime.set(Calendar.SECOND,  now.get(Calendar.SECOND));*//*
                if (now.after(alarmStartTime)) {
                    Log.e("Hey", "Added a day");
                    alarmStartTime.add(Calendar.DATE, 1);
                }
//                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmStartTime.getTimeInMillis(), AlarmManager.INTERVAL_HALF_HOUR, pendingIntent);
                if(list.size()<=5){
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmStartTime.getTimeInMillis(),
                            (sharedPreferences1.getLong("updateTime",1))*60*1000, pendingIntent);
                             Log.e(TAG, "onClick: time"+ (sharedPreferences1.getLong("updateTime",1))*60*1000);
                }
            } else {
                AlarmReceiver receiver = new AlarmReceiver();
                Objects.requireNonNull(getActivity()).unregisterReceiver(receiver);
            }*/
        }
    }

    @SuppressLint("StaticFieldLeak")
    class DisplayCurrencyValue extends AsyncTask<String, String, String> {

        StringBuffer response;
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Processing..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String urlParameters = "&source=" + from + "&currencies=" + to;
            String url = baseUrl + urlParameters;
            URL obj = null;
            try {
                obj = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection con;
            try {
                assert obj != null;
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progressDialog.dismiss();

            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("quotes");
                result = jsonObject1.getString(from + to);

                if (list.size() < 5) {
                    CreateAlertModel model = new CreateAlertModel();
                    model.setWhen("1 " + from);
                    model.setAmount(Double.parseDouble(etAlertAmount.getText().toString()));
                    model.setEquals(to);
                    model.setCurrentPrice("1 " + from + " = " + result + " " + to + " is current rate");
                    list.add(model);

                    Gson gson = new Gson();
                    String json = gson.toJson(list);
                    mEditor.putString("list", json);
                    mEditor.apply();

               /*     try {
                        mEditor.putString("list", ObjectSerializer.serialize(list));
                        mEditor.apply();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
*/
                } else {
                    Toast.makeText(getActivity(), "Maximum 5 alerts can be added", Toast.LENGTH_SHORT).show();
                }
                CreateAlertAdapter adapter = new CreateAlertAdapter(getActivity(), list);
                rvAlert.setAdapter(adapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
