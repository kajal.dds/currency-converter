package com.example.currencyconverter.fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.currencyconverter.R;
import com.example.currencyconverter.apiinterface.ApiInterface;
import com.example.currencyconverter.model.CurrencyConversion;
import com.example.currencyconverter.retrofit.ApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurrencyConversionFragment extends Fragment implements View.OnClickListener {

    private TextView tvDate, tvAns;
    private EditText etAmount;
    private String from, to, date;
    private Float amount;
    private static final String TAG = "CurrencyConversionFragm";
    private ArrayList<String> mylist = new ArrayList<>();
    private String[] temp = new String[164];
    private Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener dateDialog;
    private SharedPreferences sharedPreferences, sharedPreferences1;

    public CurrencyConversionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_currency_conversion, container, false);

        sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences("convert", MODE_PRIVATE);
        sharedPreferences1 = Objects.requireNonNull(getActivity()).getSharedPreferences("Language", Context.MODE_PRIVATE);

        etAmount = v.findViewById(R.id.et_amount);
        TextView tvConvert = v.findViewById(R.id.tv_convert);
        tvDate = v.findViewById(R.id.tv_date);
        tvAns = v.findViewById(R.id.tv_ans);
        ImageView ivDate = v.findViewById(R.id.iv_date);

        tvConvert.setOnClickListener(this);
        tvDate.setOnClickListener(this);
        ivDate.setOnClickListener(this);
        getJsonData();

        updateLabel();

        from = sharedPreferences.getString("graphSource", "USD");
        to = sharedPreferences.getString("graphDest", "INR");

        Log.e(TAG, "onCreateView: from"+from );
        Log.e(TAG, "onCreateView: to"+to );

        amount = Float.parseFloat(etAmount.getText().toString());
        date = tvDate.getText().toString();
        apicalling();

        dateDialog = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_convert:
//                from = tvFrom.getText().toString();
//                to = tvTo.getText().toString();

                if(Float.parseFloat(etAmount.getText().toString())<=0 || etAmount.getText().toString().equals("") || etAmount.getText().toString().trim().length()<=0){
                    Toast.makeText(getActivity(), "Please enter amount", Toast.LENGTH_SHORT).show();
                } else {
                    if(isConnected(Objects.requireNonNull(getActivity()))){
                        from = sharedPreferences.getString("graphSource", "USD");
                        to = sharedPreferences.getString("graphDest", "INR");

                        amount = Float.parseFloat(etAmount.getText().toString());
                        date = tvDate.getText().toString();
                        apicalling();
                    } else {
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                }

                break;

            case R.id.iv_date:
            case R.id.tv_date:
               /* new DatePickerDialog(Objects.requireNonNull(getActivity()), dateDialog, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).getDatePicker().setMaxDate(System.currentTimeMillis());*/
                DatePickerDialog datePickerDialog = new DatePickerDialog(Objects.requireNonNull(getActivity()), dateDialog, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));  //date is dateSetListener as per your code in question
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
                break;
        }


    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here YYYY-MM-DD
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
        tvDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void apicalling() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Converting");
        dialog.setCancelable(false);
        dialog.show();

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        try {
            String key = "86da42ddd38aae4c13bb5b5d44f0f437";
            Call<CurrencyConversion> call = apiInterface.getConversion(key, from, to, amount, date);
            call.enqueue(new Callback<CurrencyConversion>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(Call<CurrencyConversion> call, Response<CurrencyConversion> response) {
                    Log.e(TAG, "onResponse: url" + call.request().url());
                    CurrencyConversion data = response.body();
                    assert data != null;
                    Log.e(TAG, "onResponse: amount"+amount );
                    Log.e(TAG, "onResponse: "+sharedPreferences1.getString("mylanguage","en").equals("ar") );
                    if(sharedPreferences1.getString("mylanguage","en").equals("ar")){
                        tvAns.setText(to + " " + data.getResult() + " = " + from + " " + amount);
                    } else {
                        tvAns.setText(amount + " " + from + " = " + data.getResult() + " " + to);
                    }
                    dialog.dismiss();
                }

                @Override
                public void onFailure(Call<CurrencyConversion> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "catch: " + e.getMessage());
        }
    }


    private void showCurrencySelectionDialog(final TextView tv) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Select Currency");
            builder.setSingleChoiceItems(temp, -1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int position) {
                    dialog.dismiss();
                    try {
                        String str = mylist.get(position);
                        Log.e(TAG, "onClick: " + str);
                        tv.setText(str);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String loadJSON() {
        String json;
        try {
            InputStream is = Objects.requireNonNull(getActivity()).getAssets().open("currency");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
            Log.e(TAG, "loadJSON: response" + json);
        } catch (IOException ex) {
            ex.printStackTrace();
            Log.e(TAG, "loadJSON: catch" + ex.getMessage());
            return null;
        }
        return json;
    }

    private void getJsonData() {

        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(loadJSON());
            Log.e(TAG, "getJsonData: " + jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String currencyCode = jsonObject.getString("code");

                temp[i] = currencyCode;
                mylist.add(currencyCode);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean isConnected(Context context) {
        boolean connected;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        //we are connected to a network
        assert connectivityManager != null;
        connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
        return connected;
    }

}
