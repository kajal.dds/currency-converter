package com.example.currencyconverter.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.Nullable;

import com.example.currencyconverter.R;
import com.example.currencyconverter.model.WidgetModel;
import com.example.currencyconverter.widget.WidgetReceiver;
import com.example.currencyconverter.widget.WidgetSourceActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class WidgetService1 extends Service {

    private static final String TAG = "WidgetService";
    private ArrayList<String> currencyCodeslist = new ArrayList<>();
    private StringBuffer response;
    private RemoteViews view;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String result;
    ArrayList<WidgetModel> arrayList;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG, "onBind: ");
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.e(TAG, "onCreate: service");
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        sharedPreferences = getSharedPreferences("widget", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.apply();

        Log.e(TAG, "onStartCommand: in widget service" + WidgetSourceActivity.widgetList.size());
        Log.e(TAG, "onStartCommand: result");

        final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());

        int[] allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);

        //update the widget
        getJsonData();

        Gson gson = new Gson();
        String json = sharedPreferences.getString("list", "");
        Type type = new TypeToken<ArrayList<WidgetModel>>() {}.getType();
        arrayList = gson.fromJson(json, type);

        if(arrayList!=null && arrayList.size()>0){

            Log.e(TAG, "onStartCommand: ks check"+arrayList.size() );
            for (int i = 0; i < arrayList.size() ; i++) {
                Log.e(TAG, "onStartCommand: k check "+arrayList.get(i).getKey() + arrayList.get(i).getResult() );
            }

            if (allWidgetIds != null) {
                Log.e(TAG, "onStartCommand: l check" + allWidgetIds.length);
                for (final int widgetID : allWidgetIds) {
                    Log.e(TAG, "onStartCommand: s check " + arrayList.size());
                    for (int i = 0; i < arrayList.size(); i++) {
                        Log.e(TAG, "onStartCommand: w check" + arrayList.get(i).getWidgetId());
                        Log.e(TAG, "onStartCommand: i check" + widgetID);
                        if (arrayList.get(i).getWidgetId() == widgetID) {

                            Log.e(TAG, "onStartCommand: inn check" + arrayList.get(i).getWidgetSource() + arrayList.get(i).getWidgetDest());
                            if(isConnected(WidgetService1.this)){
                                new DisplayValue(arrayList.get(i).getWidgetSource(), arrayList.get(i).getWidgetDest()).execute();

                                Log.e(TAG, "onStartCommand: o check" + result);
                                Log.e(TAG, "onStartCommand: o i check" + widgetID);

                                Log.e(TAG, "onStartCommand: ks check"+arrayList.size() );
                                for (int j = 0; j < arrayList.size() ; j++) {
                                    Log.e(TAG, "onStartCommand: k check j "+arrayList.get(j).getKey() + arrayList.get(j).getResult() );
                                }

                                Handler handler = new Handler();
                                final int finalI = i;
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        view = new RemoteViews(getPackageName(), R.layout.widget_layout);

                                        view.setTextViewText(R.id.tv_widget_refresh,"Refresh");

                                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                                        view.setTextViewText(R.id.tv_widget_date, String.format("%s", sdf1.format(Calendar.getInstance().getTime())));
                                        Log.e(TAG, "onStartCommand: inn check result"+arrayList.get(finalI).getResult());

                                        Log.e(TAG, "run: arrayList.size() check"+arrayList.size() );
                                        view.setTextViewText(R.id.tv_widget_amount, arrayList.get(finalI).getResult());

                                        // push the update to home screen
                                        ComponentName theWidget = new ComponentName(WidgetService1.this, WidgetReceiver.class);
//                                        appWidgetManager.updateAppWidget(widgetID, view);
                                        appWidgetManager.partiallyUpdateAppWidget(widgetID, view);
                                    }
                                },4000);
                                try {
                                    Thread.sleep(500); //1s sleep
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
//           stopSelf();
            }

        }

        return START_STICKY;
    }


    private void getJsonData() {

        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(loadJSON());
            Log.e(TAG, "getJsonData: " + jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String currencyCode = jsonObject.getString("code");
                currencyCodeslist.add(currencyCode);
                Log.e(TAG, "getJsonData: " + currencyCodeslist.get(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String loadJSON() {
        String json;
        try {
            InputStream is = getAssets().open("currency");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
            Log.e(TAG, "loadJSON: response" + json);
        } catch (IOException ex) {
            ex.printStackTrace();
            Log.e(TAG, "loadJSON: catch" + ex.getMessage());
            return null;
        }
        return json;
    }

    @SuppressLint("StaticFieldLeak")
    public class DisplayValue extends AsyncTask<String, String, String> {

        String source, destination;

        DisplayValue(String source, String destination) {
            this.source = source;
            this.destination = destination;
        }

        @Override
        protected String doInBackground(String... strings) {

            Log.e(TAG, "doInBackground: " );

            String urlParameters = "&source=" +source + "&currencies=" + destination;
            String baseUrl = "https://apilayer.net/api/live?access_key=86da42ddd38aae4c13bb5b5d44f0f437";
            String url = baseUrl + urlParameters;
            URL obj = null;
            try {
                obj = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection con;
            try {
                assert obj != null;
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                BufferedReader in = new BufferedReader(

                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            /// onpostexecute
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(response.toString());
                JSONObject jsonObject1 = jsonObject.getJSONObject("quotes");
                result = "";

                result = jsonObject1.getString(source + destination);
                Log.e(TAG, "onPostExecute: check"+arrayList.size() );
                if (arrayList != null) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        Log.e(TAG, "onPostExecute: inn check key "+arrayList.get(i).getKey());
                        Log.e(TAG, "onPostExecute: inn check sd"+source+destination );
                        if (arrayList.get(i).getKey().equals(source + destination)) {
                            arrayList.get(i).setResult(result);
                            Log.e(TAG, "onPostExecute: inn check r"+result );

                            editor.clear();
                            editor.apply();

                            Gson gson = new Gson();
                            String json = gson.toJson(arrayList);
                            editor.putString("list", json);
                            editor.apply();
                        }
                        Log.e(TAG, "onPostExecute: result check"+arrayList.get(i).getKey() + "=" + arrayList.get(i).getResult() );
                    }
                }

                Gson gson1 = new Gson();
                String json1 = sharedPreferences.getString("list", "");
                Type type1 = new TypeToken<ArrayList<WidgetModel>>() {}.getType();
                arrayList = gson1.fromJson(json1, type1);

                Log.e(TAG, "onPostExecute: result" + result);
//                view.setTextViewText(R.id.tv_widget_amount, result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e(TAG, "doInBackground: over" );
            return response.toString();
        }

     /*   @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }*/
    }

    private boolean isConnected(Context context) {
        boolean connected;
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
        return connected;
    }

}
