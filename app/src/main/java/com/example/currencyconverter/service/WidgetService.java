package com.example.currencyconverter.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.Nullable;

import com.example.currencyconverter.R;
import com.example.currencyconverter.widget.WidgetReceiver;
import com.example.currencyconverter.widget.WidgetSourceActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class WidgetService extends Service {

    private static final String TAG = "WidgetService";
    private ArrayList<String> currencyCodeslist = new ArrayList<>();
    private StringBuffer response;
    private RemoteViews view;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private int id;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG, "onBind: ");
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.e(TAG, "onCreate: service");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        sharedPreferences = getSharedPreferences("widget", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.apply();

        view = new RemoteViews(getPackageName(), R.layout.widget_layout);

        Log.e(TAG, "onStartCommand: in widget service" + WidgetSourceActivity.widgetList.size());
        Log.e(TAG, "onStartCommand: result");

        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        view.setTextViewText(R.id.tv_widget_date, String.format("%s", sdf1.format(Calendar.getInstance().getTime())));


        //update the widget
        getJsonData();
        new DisplayValue().execute();
        Log.e(TAG, "onStartCommand: " + sharedPreferences.getString("response", "loading..."));

        // push the update to home screen
        ComponentName theWidget = new ComponentName(WidgetService.this, WidgetReceiver.class);
        AppWidgetManager manager = AppWidgetManager.getInstance(WidgetService.this);
        manager.updateAppWidget(theWidget, view);

        return super.onStartCommand(intent, flags, startId);
    }


    private void getJsonData() {

        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(loadJSON());
            Log.e(TAG, "getJsonData: " + jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String currencyCode = jsonObject.getString("code");
                currencyCodeslist.add(currencyCode);
                Log.e(TAG, "getJsonData: " + currencyCodeslist.get(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String loadJSON() {
        String json;
        try {
            InputStream is = getAssets().open("currency");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
            Log.e(TAG, "loadJSON: response" + json);
        } catch (IOException ex) {
            ex.printStackTrace();
            Log.e(TAG, "loadJSON: catch" + ex.getMessage());
            return null;
        }
        return json;
    }

    @SuppressLint("StaticFieldLeak")
    public class DisplayValue extends AsyncTask<String, String, String> {

        String result;

        @Override
        protected String doInBackground(String... strings) {

            String urlParameters = "&source=" + sharedPreferences.getString("src", "CAD") + "&currencies=" + sharedPreferences.getString("dest", "EUR");
            String baseUrl = "https://apilayer.net/api/live?access_key=86da42ddd38aae4c13bb5b5d44f0f437";
            String url = baseUrl + urlParameters;
            URL obj = null;
            try {
                obj = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection con;
            try {
                assert obj != null;
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                BufferedReader in = new BufferedReader(

                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(response.toString());
                JSONObject jsonObject1 = jsonObject.getJSONObject("quotes");
                result = "";
                result = jsonObject1.getString(sharedPreferences.getString("src", "USD") + sharedPreferences.getString("dest", "INR"));
                Log.e(TAG, "onPostExecute: result" + result);
                view.setTextViewText(R.id.tv_widget_amount, result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
