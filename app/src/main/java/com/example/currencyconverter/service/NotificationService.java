package com.example.currencyconverter.service;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.currencyconverter.R;
import com.example.currencyconverter.activity.MainActivity;
import com.example.currencyconverter.model.CreateAlertModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class NotificationService extends IntentService {

    private static final String TAG = "NotificationService";

    private static int NOTIFICATION_ID = 1;
    Notification notification;
    private String CHANNEL_ID;

    public NotificationService() {
        super("NotificationService");
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

      /*  getJsonData();
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG, "run: we are in service yay!" );
                new DisplayCurrencyValue().execute();
            }
        });*/

        Log.e(TAG, "alert onHandleIntent: in service");
        SharedPreferences sharedPreferences = getSharedPreferences("clickCount", MODE_PRIVATE);
        SharedPreferences.Editor mEditor = sharedPreferences.edit();
        mEditor.apply();

        Gson gson = new Gson();
        String json = sharedPreferences.getString("list", "");
        Type type = new TypeToken<ArrayList<CreateAlertModel>>() {}.getType();
        final ArrayList<CreateAlertModel> arrayList = gson.fromJson(json, type);
        Log.e(TAG, "onCreateView: arraylist" + arrayList);
        for (int i = 0; i < arrayList.size(); i++) {

            Log.e(TAG, "onCreateView: arraylist" + arrayList.get(i).getWhen().substring(2, 5));
            Log.e(TAG, "onCreateView: arraylist" + arrayList.get(i).getEquals());
            Log.e(TAG, "onCreateView: arraylist" + arrayList.get(i).getAmount());

            Handler handler = new Handler(Looper.getMainLooper());
            final int finalI = i;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    new DisplayCurrencyValue(arrayList.get(finalI).getWhen().substring(2, 5), arrayList.get(finalI).getEquals(), arrayList.get(finalI).getAmount()).execute();
                }
            });
        }
    }


    @SuppressLint("StaticFieldLeak")
    class DisplayCurrencyValue extends AsyncTask<String, String, String> {

        StringBuffer response;
        String source, destination;
        double amountt;

        DisplayCurrencyValue(String from, String to, double amount) {
            this.source = from;
            this.destination = to;
            this.amountt = amount;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            String urlParameters = "&source=" + source + "&currencies=" + destination;
            String baseUrl = "https://apilayer.net/api/live?access_key=86da42ddd38aae4c13bb5b5d44f0f437";
            String url = baseUrl + urlParameters;
            URL obj = null;
            try {
                obj = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection con;
            try {
                assert obj != null;
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonObject1 = jsonObject.getJSONObject("quotes");
                String result = jsonObject1.getString(source + destination);

                Log.e(TAG, "onPostExecute: amount" + amountt);
                Log.e(TAG, "onPostExecute: result" + result);

                if (Double.parseDouble(result) >= amountt || Double.parseDouble(result) <= amountt) {
                    Log.e(TAG, "onPostExecute: yay alert user now");

                    NotificationManager notificationManager1 = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    Intent mIntent = new Intent(NotificationService.this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("test", "test");
                    mIntent.putExtras(bundle);
                    PendingIntent pendingIntent = PendingIntent.getActivity(NotificationService.this, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                    Resources res = getResources();
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
                    Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                    notification = new Notification.Builder(NotificationService.this)
                            .setContentIntent(pendingIntent)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.ic_launcher_background))
                            .setTicker("ticker value")
                            .setAutoCancel(true)
                            .setShowWhen(true)
                            .setWhen(System.currentTimeMillis())
//                .setPriority(8)
                            .setSound(soundUri)
                            .setContentTitle("Currency Converter")
                            .setContentText("Rate alert->  1" + source + " = " + amountt + " " + destination).build();
                    notification.flags |= Notification.FLAG_AUTO_CANCEL | Notification.FLAG_SHOW_LIGHTS;
                    notification.defaults |= Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
                    notification.ledARGB = 0xFFFFA500;
                    notification.ledOnMS = 800;
                    notification.ledOffMS = 1000;
                    notificationManager1 = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    assert notificationManager1 != null;
                    notificationManager1.notify(NOTIFICATION_ID, notification);
                    NOTIFICATION_ID++;
                    Log.e("notif", "Notifications sent.");

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        CharSequence name = "Currency Converter";
                        String description = "Alert Alert";
                        int importance = NotificationManager.IMPORTANCE_DEFAULT;
                        NotificationChannel channel = new NotificationChannel(NotificationChannel.DEFAULT_CHANNEL_ID, name, importance);
                        channel.setDescription(description);
                        channel.enableVibration(true);
                        AudioAttributes attributes = new AudioAttributes.Builder()
                                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                                .build();
                        channel.setSound(soundUri, attributes);
                        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                        NotificationManager notificationManager = getSystemService(NotificationManager.class);
                        assert notificationManager != null;
                        notificationManager.createNotificationChannel(channel);
                        notificationManager.notify(NOTIFICATION_ID, notification);
                        NOTIFICATION_ID++;
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

    /*public void createNotification(String message) {

        CHANNEL_ID = getResources().getString(R.string.app_name);
        if (message != null ) {
            createNotificationChannel();

            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle(UiUtil.getStringSafe(R.string.app_name))
                    .setContentText(message)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);
            Uri uri =RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            mBuilder.setSound(uri);


            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            int notificationId = (int) (System.currentTimeMillis()/4);
            notificationManager.notify(notificationId, mBuilder.build());
        }

}*/
